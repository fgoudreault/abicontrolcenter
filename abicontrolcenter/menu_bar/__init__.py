import json

from .menu_bar import AbiControlCenterMenuBar
from ..screens import __ALL_SCREENS__


__ALL_MENU_BAR_KV_FILES__ = [
        "menu_bar.kv",
        ]
__ALL_MENU_BAR_SETTINGS__ = json.dumps([
        {"type": "screenorderer",
         "title": "Menu bar order",
         "desc": "Change the order of the menu bar buttons.",
         "section": "menu-bar.buttons",
         "key": "order",
        },
        ])
__ALL_MENU_BAR_DEFAULT_SETTINGS__ = {
        "menu-bar.buttons": {
            "order": list(__ALL_SCREENS__.keys()),
            },
        }
