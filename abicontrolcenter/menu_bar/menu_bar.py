from kivy.app import App
from kivy.properties import ObjectProperty
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.stacklayout import StackLayout

from ..custom_widgets import BackgroundColor
from ..routines import get_user_config
from ..screens import __ALL_SCREENS__


class MenuLabel(Label, BackgroundColor):
    """Menu Label class.
    """
    # background_color = ListProperty(__MENU_LABEL_BACKGROUND_COLOR__)


class AbiControlCenterMenuBar(RelativeLayout):
    """The abicontrolcenter left menu bar.
    """
    screen_buttons = ObjectProperty(None)

    def __getattr__(self, attr):
        if attr.startswith("button_for_"):
            return getattr(self.screen_buttons, attr)
        super().__getattr__(attr)

    @property
    def current_screen(self):
        return self.screen_manager.current

    @current_screen.setter
    def current_screen(self, current):
        # unpress last scr button
        last_button = self.get_menu_button(self.current_screen)
        last_button.state = "normal"
        # make the corresponding button pressed
        new_btn = self.get_menu_button(current)
        new_btn.state = "down"
        # change screen
        if current != "settings":
            # settings is an exception
            self.screen_manager.current = current

    @property
    def screen_manager(self):
        app = App.get_running_app()
        return app.screen_manager

    def get_menu_button(self, screen):
        """Get the menu bar button corresponding to the given screen.
        """
        if screen not in __ALL_SCREENS__:
            raise ValueError(f"Not a screen: '{screen}'.")
        return getattr(self, f"button_for_{screen}_screen")

    def get_transition_direction(self, current, new):
        """Computes the transition direction from the current screen to the
        new screen.

        Parameters
        ----------
        current, new: str
            The current and the new screen names.
        """
        screen_order = get_user_config("menu-bar.buttons", "order", dtype=list)
        cur_scr = screen_order.index(current)
        new_scr = screen_order.index(new)
        if cur_scr > new_scr:
            return "down"  # we go up so we slide down
        else:
            return "up"

    def switch_to_screen(self, screen):
        """Switch to a given screen.

        Parameters
        ----------
        screen: str
            The screen name to switch to.
        """
        # get the relative position of current screen to new screen
        scr_man = self.screen_manager
        scr_man.transition.direction = self.get_transition_direction(
                self.current_screen, screen)
        self.current_screen = screen
        if screen == "settings":
            # open settings
            App.get_running_app().open_settings()


class ScreenButtons(StackLayout):
    """This is where the screen buttons are displayed.

    This is coded in python since the number of buttons depends
    on what is available in the system.
    """
    menu_bar = ObjectProperty(None)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.menu_bar_buttons = []
        self.screen_switchers = {}
        self.create_menu_bar_buttons()

    def create_menu_bar_buttons(self):
        """(Re)create menu bar buttons.
        """
        settings = False
        if self.menu_bar_buttons:
            # remove previous buttons
            # this is called when we change the screen order in the settings
            for btn in self.menu_bar_buttons:
                self.remove_widget(btn)
            del self.menu_bar_buttons
            self.menu_bar_buttons = []
            settings = True
        from ..app_settings import __MENU_BAR_BUTTON_HEIGHT__
        screen_order = get_user_config("menu-bar.buttons", "order", dtype=list)
        for idx, screen_name in enumerate(screen_order):
            state = "normal"
            if (not idx and not settings) or (settings and
                                              screen_name == "settings"):
                state = "down"  # first screen by default
            # create button widget
            btn = Button(
                    size_hint=(1, None),
                    state=state,
                    text=screen_name.capitalize(),
                    height=__MENU_BAR_BUTTON_HEIGHT__,
                    # id=f"button_for_{screen_name}_screen"
                    )
            self.menu_bar_buttons.append(btn)
            # create method that will call the menu_bar method to switch screen
            # add this method to self
            self.screen_switchers.setdefault(
                    screen_name, ScreenSwitcher(screen_name))
            # setattr(self, f"switch_to_{screen_name}_screen", switch_func)
            # bind this method to the button
            btn.bind(
                on_release=self.screen_switchers[screen_name].switch_to_screen)
            self.add_widget(btn)
            # add the button to self
            setattr(self, f"button_for_{screen_name}_screen", btn)


class ScreenSwitcher:
    def __init__(self, screen_name):
        self.screen_name = screen_name

    def switch_to_screen(self, instance):
        App.get_running_app().menu_bar.switch_to_screen(self.screen_name)
