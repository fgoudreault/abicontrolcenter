import os

# import playsound  # only works with python2 on Mac?
try:
    import notify2
except ModuleNotFoundError:
    __HAVE_NOTIFY2__ = False
else:
    __HAVE_NOTIFY2__ = True
import simpleaudio

from .pomodoro_popup import PomodoroTimesUpPopup
from ..._paths import __ABICC_SOUNDS_DIR__, __ABICC_LOGO_DIR__
from ...routines import get_user_config


class PomodoroAlarm:
    """Alarm object handled by the PomodoroTimer object to notify user
    when pomodoro timer runs out.
    """
    def __init__(self):
        self.popup = None
        self.notification = None

    @property
    def is_notification_open(self):
        """Returns True if the notification is open.
        """
        return self.notification is not None

    @property
    def is_popup_open(self):
        """Returns True if a popup Window is open.
        """
        if self.popup is None:
            return False
        return self.popup.is_popup_open

    @property
    def sound_alarm_on(self):
        return get_user_config("pomodoro.alarm", "pomodoro_alarm") == "1"

    @property
    def notification_on(self):
        return get_user_config(
                "pomodoro.alarm", "pomodoro_notification") == "1"

    @property
    def sound_alarm_file(self):
        return os.path.join(__ABICC_SOUNDS_DIR__, "pomodoro_alarm.wav")

    def dismiss_notification(self):
        """Dismisses the notification if it's open.
        """
        if not self.is_notification_open:
            return
        self.notification.close()
        del self.notification
        self.notification = None

    def dismiss_popup(self):
        """Dismiss the popup if it's open.
        """
        if not self.is_popup_open:
            # nothing to do
            return
        self.popup.dismiss()
        # delete popup
        del self.popup
        self.popup = None

    def play_sound_alarm(self):
        """play the alarm sound.
        """
        wave_obj = simpleaudio.WaveObject.from_wave_file(self.sound_alarm_file)
        play_obj = wave_obj.play()
        play_obj.wait_done()
        # playsound(self.sound_alarm_file)

    def send_notification(
            self, work_state, pomodoro_total_break_minutes,
            pomodoro_total_work_minutes):
        """Sends a notification to the user.

        Parameters
        ----------
        work_state : str, {'working', 'break'}
            The current working state.
        pomodoro_total_break_minutes : int
            Total number of break minutes.
        pomodoro_total_work_minutes : int
            Total number of work minutes.
        """
        if not __HAVE_NOTIFY2__:
            # no notifications unfortunately...
            return
        notify2.init("abicontrolcenter")
        if work_state == "working":
            desc = ("It is now time for your break ("
                    f"{pomodoro_total_break_minutes} min)!")
        elif work_state == "break":
            desc = ("It is now time to work ("
                    f"{pomodoro_total_work_minutes} min)!")
        else:
            raise ValueError(work_state)
        self.notification = notify2.Notification(
                "AbiControlCenter: Pomodoro",
                desc,
                os.path.join(__ABICC_LOGO_DIR__, "app_icon.png"))
        self.notification.show()

    def show_popup(self, work_state, callback):
        """Show the alarm popup.

        Parameters
        ----------
        work_state : str
            The current work_state.
        callback : function
            The callback function when the 'restart' button is pressed.
        """
        self.popup = PomodoroTimesUpPopup(
                work_state, callback, size_hint=(1, 1))
        self.popup.open()
