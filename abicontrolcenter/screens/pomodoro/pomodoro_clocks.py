import abc
import datetime

from kivy.app import App
from kivy.graphics import Color, Line
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.logger import Logger
from kivy.properties import ObjectProperty

from . import __POMODORO_POPUP_SIZE__
from .pomodoro_alarm import PomodoroAlarm
from ...app_settings import (
        __MENU_BAR_BUTTON_HEIGHT__,
        )
from ...routines import get_user_config


__MONTHS__ = [
        "January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December",
        ]
__WEEKDAYS__ = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
                "Saturday", "Sunday"]
__IMG_SIZE__ = (
        __POMODORO_POPUP_SIZE__[0],
        __POMODORO_POPUP_SIZE__[1] - 2 * __MENU_BAR_BUTTON_HEIGHT__ - 10)


class BasePomodoroClock(Label):
    """Base class for a pomodoro clock.
    """
    _dt = None

    def __init__(self, *args, **kwargs):
        Label.__init__(self, *args, **kwargs)
        self.update()
        # set text every 1 seconds
        self._clock = Clock.schedule_interval(self.update, self._dt)

    @abc.abstractmethod
    def update(self, *args, **kwargs):
        pass


class PomodoroClock(BasePomodoroClock):
    """The pomodoro clock label.
    """
    _dt = 0.5  # 0.5 second interval for the blinking aspect

    def __init__(self, *args, **kwargs):
        self._show_colon = True
        super().__init__(*args, **kwargs)

    def update(self, *args):
        time = self.get_time()
        self.text = f"Clock: {self.get_date()} {time}"

    def get_date(self):
        now = datetime.datetime.now()
        day = now.day
        if day in (1, 21, 31):
            day = str(day) + "st"
        elif day in (2, 22):
            day = str(day) + "nd"
        elif day in (3, 23):
            day = str(day) + "rd"
        else:
            day = str(day) + "th"
        return (f"{__WEEKDAYS__[now.isoweekday() - 1]} {day} of "
                f"{__MONTHS__[now.month - 1]}, {now.year}")

    def get_time(self):
        now = datetime.datetime.now()
        if self._show_colon:
            # uncomment to allow colon flashing at each second
            # self._show_colon = False
            return f"{str(now.hour).zfill(2)}:{str(now.minute).zfill(2)}"
        else:
            self._show_colon = True
            return f"{str(now.hour).zfill(2)} {str(now.minute).zfill(2)}"


class PomodoroTimer(BasePomodoroClock):
    """The pomodoro timer clock.
    """
    work_status_label = ObjectProperty(None)
    pomodoro_counter = ObjectProperty(None)
    # make _dt small enough. if _dt = 0.01, every 1 / 0.01 seconds there will
    # be a small lag in the clock
    _dt = 0.01

    def __init__(self, *args, **kwargs):
        # angle start for the timer circle
        # set base utility first
        self._timer_start_time = datetime.datetime.now()
        # this var will switch back and forth True/False when blinking happens
        self._has_blinked = False
        self.start_time = datetime.datetime.now()
        self.popupWindow = None
        self.notification = None
        self.state = "idle"
        self.work_state = "working"
        self.alarm = PomodoroAlarm()
        self.circle_color = get_user_config(
                "pomodoro.appearance", "pomodoro_circle_color", dtype=list)
        self.reset(update_timer_circle=False)
        # super init at the end since it will ask for the properties above
        super().__init__(*args, **kwargs)
        self.draw_timer_circle(self.pomodoro_total_work_seconds)

    @property
    def pomodoro_total_work_seconds(self):
        return self.pomodoro_total_work_minutes * 60

    @property
    def pomodoro_work_minutes(self):
        return self.pomodoro_total_work_minutes - self.pomodoro_work_hours * 60

    @property
    def pomodoro_work_hours(self):
        return self.pomodoro_total_work_minutes // 60

    @property
    def pomodoro_total_work_minutes(self):
        return int(get_user_config("pomodoro.work-and-break-times",
                                   "pomodoro_work_total_minutes"))

    @property
    def pomodoro_total_break_seconds(self):
        return self.pomodoro_total_break_minutes * 60

    @property
    def pomodoro_break_minutes(self):
        return (
            self.pomodoro_total_break_minutes - self.pomodoro_break_hours * 60)

    @property
    def pomodoro_break_hours(self):
        return self.pomodoro_total_break_minutes // 60

    @property
    def pomodoro_total_break_minutes(self):
        return int(get_user_config("pomodoro.work-and-break-times",
                                   "pomodoro_break_total_minutes"))

    @property
    def work_string(self):
        if self.state == "running":
            if self.work_state == "working":
                title = "You should be WORKING"
            else:
                title = "Break"
        elif self.state == "paused":
            title = "PAUSED"
        elif self.state == "idle":
            title = "Press start"
        return title

    def update(self, *args, update_timer_circle=True):
        if self.work_status_label is not None:
            self.work_status_label.text = self.work_string
        remaining_time = self.get_time()  # remaining seconds
        if update_timer_circle:
            # redraw timer circle
            self.draw_timer_circle(remaining_time)
        # clock string
        remaining_clock = self.get_remaining_clock(remaining_time)
        # update text
        # if negative time: blink main pomodoro timer but not in the menu bar
        new_text = remaining_clock
        if remaining_time < 0:
            # time's up call alarm
            if self.alarm.popup is None:
                self.call_alarm()
            if not self._has_blinked:
                new_text = ""
                self._has_blinked = True
            else:
                self._has_blinked = False
        self.text = new_text
        # if running also set the menu bar button accordingly
        app = App.get_running_app()
        if not hasattr(app, "menu_bar"):
            # don't do anything else in case the menu bar is not created yet
            return
        btn = app.menu_bar.button_for_pomodoro_screen
        if self.state == "running":
            btn.text = f"Pomodoro ({remaining_clock})"
        elif self.state == "idle":
            btn.text = "Pomodoro"

    def get_time(self):
        # compute remaining time
        if self.state == "paused" or self.state == "idle":
            remain_total_seconds = self.remaining_total_seconds
        if self.state == "running":
            delta = datetime.datetime.now() - self.start_time
            elapsed_seconds = delta.total_seconds()
            remain_total_seconds = (
                    self.remaining_total_seconds - elapsed_seconds)
        return remain_total_seconds

    def get_remaining_clock(self, remain_total_seconds):
        """Returns timer from the remaining seconds
        """
        if remain_total_seconds >= 0:
            remain_hrs = remain_total_seconds // 3600
            remain_mins = remain_total_seconds // 60 - remain_hrs * 60
            remain_secs = (
                    remain_total_seconds - remain_mins * 60 - remain_hrs * 3600
                    )
            return (f"{str(int(remain_hrs)).zfill(2)}:"
                    f"{str(int(remain_mins)).zfill(2)}:"
                    f"{str(int(remain_secs)).zfill(2)}")
        # compute negative time
        neg_hours = -remain_total_seconds // 3600
        neg_minutes = -remain_total_seconds // 60 - neg_hours * 60
        neg_seconds = (
                -remain_total_seconds - neg_minutes * 60 -
                neg_hours * 3600)
        return (f"-{str(int(neg_hours)).zfill(2)}:"
                f"{str(int(neg_minutes)).zfill(2)}:"
                f"{str(int(neg_seconds)).zfill(2)}")

    def start_pause(self):
        """Start or pause the timer.
        """
        Logger.debug("Pomodoro Timer: Pressing start/pause button.")
        if self.state == "paused" or self.state == "idle":
            self.state = "running"
            self.start_time = datetime.datetime.now()
        elif self.state == "running":
            self.remaining_total_seconds -= (
                    datetime.datetime.now() - self.start_time).total_seconds()
            self.state = "paused"

    def call_alarm(self):
        """When pomodoro timer hits 0, call the alarm to notify user.
        """
        # also play a sound if allowed in settings
        if self.alarm.sound_alarm_on:
            self.alarm.play_sound_alarm()
        # send a notification if allowed in settings
        if self.alarm.notification_on:
            self.alarm.send_notification(
                    self.work_state, self.pomodoro_total_break_minutes,
                    self.pomodoro_total_work_minutes)
        # show popup
        self.alarm.show_popup(self.work_state, self.switch_work_state)

    def reset(self, full_reset=False, **kwargs):
        """Reset PomodoroTimer. If self.work_state is 'working', it resets
        to the work timer and if self.work_state is 'break' then it resets
        the break timer.

        Parameters
        ----------
        full_reset: bool
            If True, resets the pomodoro work state to 'working'.
        """
        if full_reset:
            self.work_state = "working"
        if self.work_state == "working":
            self.remaining_total_seconds = (
                self.pomodoro_work_minutes * 60 +
                self.pomodoro_work_hours * 3600)
        elif self.work_state == "break":
            self.remaining_total_seconds = (
                    self.pomodoro_break_minutes * 60 +
                    self.pomodoro_break_hours * 3600
                    )
        else:
            raise Exception(
                    f"Invalid PomodoroTimer work_state: '{self.work_state}'.")
        self.state = "idle"
        # we force the button rebelling in case we triggered this function
        # when changing settings
        self.update(**kwargs)

    def switch_work_state(self):
        """Callback of the Popup ok button or the notification.

        Switches the work state when the timer resumes.
        """
        # switch work state
        self.work_state = "working" if self.work_state == "break" else "break"
        # reset timer with new work state
        self.reset()
        # dismiss popup if there was one and reset to None
        if self.alarm.is_popup_open:
            self.alarm.dismiss_popup()
        # if there is a notification kill it
        if self.alarm.is_notification_open:
            self.alarm.dismiss_notification()
        # increase pomodoro counter when pomodoro is completed
        if self.work_state == "break":
            # we switched from working
            self.pomodoro_counter.increment()
        # start timer
        self.start_pause()  # start timer

    def draw_timer_circle(self, remaining_time):
        """Update the time circle according to the remaining time
        """
        # if self.timer_circle is None:
        #     # do nothing yet
        #     return
        if remaining_time < 0:
            new_angle = 0
        else:
            if self.work_state == "working":
                new_angle = (
                        remaining_time / self.pomodoro_total_work_seconds
                        ) * 360
            else:
                new_angle = (
                        remaining_time / self.pomodoro_total_break_seconds
                        ) * 360
        # use canvas.before to not erase the pomodoro timer when using clear
        with self.canvas.before:
            self.canvas.before.clear()
            Color(rgba=self.circle_color)
            Line(ellipse=(
                self.center_x - 150, self.center_y - 155, 300, 300, 0,
                new_angle),
                 width=3.0)
