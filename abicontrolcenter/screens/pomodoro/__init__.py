import json
import os

from .pomodoro_screen import PomodoroScreen
from ..._paths import __ABICC_STATIC_DIR__


__POMODORO_POPUP_SIZE__ = (300, 300)
__ALL_POSSIBLE_POMODORO_WORK_STATES__ = ["working", "break"]
__ALL_POMODORO_DATA__ = {
        "kv_files": [
            "pomodoro_screen.kv",
            "pomodoro_popup.kv"
            ],
        "screen_cls": PomodoroScreen,
        "settings": json.dumps([
            # pomodoro work times
            {"type": "title",
             "title": "Work and break durations."},
            {"type": "numeric",
             "title": "Pomodoro Working Time (total minutes)",
             "desc": ("Sets the number of total minutes of work in one Pomodoro "
                      "session."),
             "key": "pomodoro_work_total_minutes",
             "section": "pomodoro.work-and-break-times"
             },
            {"type": "numeric",
             "title": "Pomodoro Break Time (total minutes)",
             "desc": ("Sets the number of total minutes of break in one Pomodoro "
                      "session."),
             "key": "pomodoro_break_total_minutes",
             "section": "pomodoro.work-and-break-times"
             },
            # pomodoro appearance
            {"type": "title",
             "title": "Pomodoro appearance"},
            {"type": "colorpicker",  # custom type
             "title": "Pomodoro circle color",
             "desc": "Change the color of the pomodoro circle.",
             "section": "pomodoro.appearance",
             "key": "pomodoro_circle_color",
             },
            # pomodoro sound alarm
            {"type": "title",
             "title": "Alarm/Notification settings"},
            {"type": "bool",
             "title": "Pomodoro alarm",
             "desc": "Play an alarm when pomodoro timer is expired.",
             "section": "pomodoro.alarm",
             "key": "pomodoro_alarm",
             },
            {"type": "bool",
             "title": "Pomodoro Notification",
             "desc": "A notification appears when timer is expired.",
             "section": "pomodoro.alarm",
             "key": "pomodoro_notification",
             },
            {"type": "title",
             "title": "Pomodoro Motivational Pictures",
             },
            {"type": "bool",
             "title": "Enable Motivational Pictures.",
             "section": "pomodoro.motivational-pictures",
             "key": "pomodoro_show_motivational_pictures",
             },
            {"type": "path",
             "title": "Motivational Picture directory (lil cats)",
             "desc": ("A path to a directory containing motivational pictures that"
                      " appears when the pomodoro timer runs out."),
             "section": "pomodoro.motivational-pictures",
             "key": "pomodoro_motivational_pictures_dir",
             "dirselect": True,
             "show_hidden": False,
             },
            ]),
        "default_settings": {
        "pomodoro.work-and-break-times": {
            "pomodoro_work_total_minutes": 50,
            "pomodoro_break_total_minutes": 10
            },
        "pomodoro.alarm": {
            "pomodoro_alarm": 1,
            "pomodoro_notification": 1,
            },
        "pomodoro.motivational-pictures": {
            "pomodoro_show_motivational_pictures": 1,
            "pomodoro_motivational_pictures_dir": os.path.join(
                __ABICC_STATIC_DIR__, "lil_cats"),
            },
        "pomodoro.appearance": {
            "pomodoro_circle_color": [0, 1, 0, 1],
            },
        },
        }
