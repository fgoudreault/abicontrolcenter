import os
import random

from kivy.properties import ObjectProperty

from . import __ALL_POSSIBLE_POMODORO_WORK_STATES__, __POMODORO_POPUP_SIZE__
from ...bases import BasePopup
from ...routines import get_user_config


class PomodoroTimesUpPopup(BasePopup):
    """Class for a popup that shows when the time's up."""
    image = ObjectProperty(None)
    textlabel = ObjectProperty(None)
    start_button = ObjectProperty(None)

    def __init__(
            self, work_state, switch_work_state_callback, *args, **kwargs):
        """PomodoroTimesUpPopup init method.

        Parameters
        ----------
        work_state : str
            The current working state.
        switch_work_state_callback : function
            The function called when the 'restart' button is pressed.
        """
        if work_state not in __ALL_POSSIBLE_POMODORO_WORK_STATES__:
            raise ValueError(work_state)
        self.work_state = work_state
        self.switch_work_state_callback = switch_work_state_callback
        self._last_img_index = None
        super().__init__(*args, **kwargs)

    @property
    def show_motivational_picture(self):
        return get_user_config(
                "pomodoro.motivational-pictures",
                "pomodoro_show_motivational_pictures") == "1"

    def get_image_source(self):
        img_dir = get_user_config("pomodoro.motivational-pictures",
                                  "pomodoro_motivational_pictures_dir")
        all_files = os.listdir(img_dir)
        all_files = [x for x in all_files
                     if x.lower().endswith(".png") or
                     x.lower().endswith(".jpg")]
        all_images = [os.path.join(img_dir, x) for x in all_files]
        if not len(all_images):
            # no images to show
            return None
        if len(all_images) > 1:
            # choose randomly such that the image differs from the last one
            rand = self._last_img_index
            while rand == self._last_img_index:
                rand = random.randint(0, len(all_images) - 1)
            imgpath = all_images[rand]
            self._last_img_index = rand
        else:
            imgpath = all_images[0]
        return imgpath

    def get_label_text(self):
        """Compute the label text.
        """
        txt = "Time's up!"
        if not self.show_motivational_picture:
            txt += "\n\n"
        else:
            txt += " "
        if self.work_state == "working":
            txt += "Time to take a break!"
        else:
            txt += "Time to work!"
        if self.show_motivational_picture:
            txt += "\nHere's a motivational image:"
        return txt

    def open(self):
        super().open("Times's Up!", size=__POMODORO_POPUP_SIZE__)
