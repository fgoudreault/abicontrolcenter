import json
import os

from .statistics_screen import StatisticsScreen


__ALL_STATISTICS_DATA__ = {
        "kv_files": [
            "statistics_screen.kv",
            ],
        "screen_cls": StatisticsScreen,
        "settings": json.dumps([
            {"type": "options",
             "title": "Beginning of the week",
             "desc": "Day of the week on which the week begins.",
             "key": "begin_weekday",
             "section": "statistics.general",
             "options": ["Monday", "Tuesday", "Thursday", "Wednesday", "Thursday",
                         "Friday", "Saturday", "Sunday"],
             },
             {"type": "path",
              "title": "Data file path",
              "desc": "The path to the data file continaing the stats.",
              "key": "datafilepath",
              "section": "statistics.general",
              "dirselect": False,
              "show_hidden": False,
              },
             ]),
        "default_settings": {
            "statistics.general": {
                "begin_weekday": "Monday",
                "datafilepath": os.path.join(
                    os.path.expanduser("~"), ".cache",
                    "abicontrolcenter_stats.dat"),
                },
            },
        }
