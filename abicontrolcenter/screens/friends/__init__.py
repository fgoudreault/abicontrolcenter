import json

from .friends_screen import FriendsScreen


__ALL_FRIENDS_DATA__ = {
        "kv_files": [
            "friends_screen.kv",
            ],
        "screen_cls": FriendsScreen,
        "settings": json.dumps([]),
        "default_settings": {},
        }
