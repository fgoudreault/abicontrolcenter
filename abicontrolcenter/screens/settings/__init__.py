import json

from .settings_screen import (
        AbiControlCenterSettingsScreen,  # SettingsButton,
        SettingsScreen,
        )
from .custom_settings_types import SettingColorPicker, SettingScreenOrderer
from .max_window_size_exceeded_popup import MaxWindowSizeExceededPopup


__ALL_SETTINGS_DATA__ = {
        "kv_files": [
            "settings_screen.kv",
            "custom_settings_types.kv",
            "max_window_size_exceeded_popup.kv",
            ],
        "screen_cls": SettingsScreen,
        "settings": json.dumps([]),
        "default_settings": {},
        }
__CUSTOM_SETTINGS_TYPES__ = {
        "colorpicker": SettingColorPicker,
        "screenorderer": SettingScreenOrderer,
        }
