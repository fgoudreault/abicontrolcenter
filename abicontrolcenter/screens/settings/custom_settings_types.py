from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.settings import SettingItem
from kivy_garden.drag_n_drop import (
        DraggableObjectBehavior, DraggableLayoutBehavior,
        )

from ...bases import BasePopup
from ...routines import get_user_config

# #############################################################################
# ################## color picker #############################################
# #############################################################################


class ColorPickerPopup(BasePopup):
    """A popup for a color picker.
    """
    color_picker = ObjectProperty(None)

    def __init__(self, color_setting_item, *args, **kwargs):
        self.color_setting_item = color_setting_item
        super().__init__(*args, **kwargs)
        self.color_picker.bind(color=self.on_color)
        self.initial_color = self.color_setting_item.color  # save initial

    @property
    def color(self):
        return [x for x in self.color_picker.color]

    def on_color(self, instance, value):
        # change color of settings color label
        self.color_setting_item.color = value

    def dismiss(self, save=True):
        if save:
            # dismiss popup and save color
            self.color_setting_item.value = self.color
        else:
            # return to initial color
            self.color_setting_item.color = self.initial_color
        super().dismiss()

    def open(self):
        super().open("Pick a color.", auto_dismiss=True, size=(400, 500))


class SettingColorPicker(SettingItem):
    """Custom Setting type to get a color picker to choose a color.
    """
    def __init__(self, *args, app=None, **kwargs):
        self.register_event_type("on_release")
        super().__init__(*args, **kwargs)
        # color picker is a colored rectangle, but once it's clicked,
        # the color picker is opened
        # the color is get from the config
        self.app = app
        color = get_user_config(
                "pomodoro.appearance", "pomodoro_circle_color",
                dtype=list,
                app=self.app)
        self.button = Button(
                text="click", font_size="15sp", background_normal="",
                background_color=color)
        self.button.bind(on_press=self.on_button_pressed)
        self.add_widget(self.button)

    @property
    def color(self):
        return self.button.background_color

    @color.setter
    def color(self, color):
        self.button.background_color = color

    def on_button_pressed(self, instance):
        # open a popup with a color picker
        picker = ColorPickerPopup(self)
        picker.open()


# #############################################################################
# ########################### screen orderer ##################################
# #############################################################################

def ordered_dict_keys_from_values(dic):
    """Returns the ordered list of keys based on the values.

    Parameters
    ----------
    dic: dict
        The dictionary who we want to sort the keys.

    Returns
    -------
    list: the sorted list of keys.
    """
    return [k for k, v in sorted(dic.items(), key=lambda item: item[1])]


class ScreenOrderer(DraggableLayoutBehavior, RelativeLayout):
    """The draggable box.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # add draggable labels corresponding to the screens
        self.dragable_labels = []
        # intial positions
        from ... import __ABISUITE_AVAILABLE__
        from ...app_settings import __MENU_BAR_BUTTON_HEIGHT__
        self.screen_order = get_user_config(
                "menu-bar.buttons", "order", dtype=list)
        for screen_name in reversed(self.screen_order):
            if not __ABISUITE_AVAILABLE__ and screen_name == "abisuite":
                continue
            label = DragableLabel(
                        screen_name=screen_name,
                        text=screen_name.capitalize() + " screen",
                        pos=(0, (len(self.dragable_labels) *
                                 __MENU_BAR_BUTTON_HEIGHT__)),
                        height=__MENU_BAR_BUTTON_HEIGHT__,
                        size_hint=(1, None))
            self.dragable_labels.append(label)
            self.add_widget(label)

    def reposition_labels(self, screen_order, omit_screen=None):
        from ...app_settings import __MENU_BAR_BUTTON_HEIGHT__
        # get new height according to new screen_order
        new_ys = [(screen_order.index(lab.screen_name) *
                   __MENU_BAR_BUTTON_HEIGHT__)
                  for lab in self.dragable_labels]
        self.screen_order = screen_order  # store this in memory
        for new_y, label in zip(new_ys, self.dragable_labels):
            label.y = new_y
            if label.screen_name == omit_screen:
                label.text = ""
            else:
                label.text = label.screen_name.capitalize() + " screen"

    def get_drop_insertion_index_move(self, *args):
        # override this method to not add a 'spacer widget' that appears where
        # on the layout when a dragged item hovers the layout
        pass


class DragableLabel(DraggableObjectBehavior, Label):
    """Draggable label
    """
    screen_name = StringProperty(None)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # touch position when start to grab
        self.touch_pos = None
        # position when starting to grab (used to compute new pos)
        self.pos_before_grab = None

    def on_touch_down(self, touch):
        super().on_touch_down(touch)
        if not self.collide_point(*touch.pos):
            return
        self.touch_pos = touch.pos
        self.pos_before_grab = (self.x, self.y)

    def on_touch_up(self, touch):
        if self.touch_pos is None:
            return
        # below is a copy paste from:
        # https://github.com/kivy-garden/drag_n_drop/blob/d1e0fc3e30f0930dd5258548b61701c53da28e19/kivy_garden/drag_n_drop/__init__.py#L235
        # I don't know why but calling super() does not work
        # if we 'leave' the popup, the preview stays in the main window
        # but copy pasting the code seems to work...
        uid = self._touch_uid()
        if uid not in touch.ud:
            touch.ud[uid] = False
            return super(DraggableObjectBehavior, self).on_touch_up(touch)

        if not touch.ud[uid]:
            return super(DraggableObjectBehavior, self).on_touch_up(touch)

        if touch.grab_current is not self:
            return False

        touch.ungrab(self)
        self._drag_touch = None

        to_rtn = self.drag_controller.drag_up(self, touch)
        # recompute position of labels
        self.parent.reposition_labels(
                self._get_new_screen_order(self._get_new_pos(touch)))
        self.touch_pos = None
        self.pos_before_grab = None
        return to_rtn

    def on_touch_move(self, touch):
        # when moving the draggable label, move the other labels to show where
        # it will be dropped when we drop it
        # store pos and restore
        super().on_touch_move(touch)
        if self.touch_pos is None:
            return
        screen_order = self._get_new_screen_order(self._get_new_pos(touch))
        self.parent.reposition_labels(
                screen_order, omit_screen=self.screen_name)

    def _get_new_pos(self, touch):
        # get new position coordinates according to where mouse is
        new_x = self.x  # doesn't change
        new_y = self.pos_before_grab[1] + touch.pos[1] - self.touch_pos[1]
        return (new_x, new_y)

    def _get_new_screen_order(self, newpos):
        # get new screen order according to the widget at its new position
        under = {}
        above = {}
        for label in self.parent.dragable_labels:
            if label.screen_name == self.screen_name:
                continue
            if label.y < newpos[1]:
                under[label.screen_name] = label.y
            else:
                above[label.screen_name] = label.y
        # sort screens
        # inspired from:
        # https://stackoverflow.com/a/613218/6362595
        return (ordered_dict_keys_from_values(under) + [self.screen_name] +
                ordered_dict_keys_from_values(above))


class ScreenOrderingPopup(BasePopup):
    """Popup to order screens.
    """
    screen_orderer = ObjectProperty(None)

    def __init__(self, screen_orderer_setting_item, *args, **kwargs):
        self.screen_orderer_setting_item = screen_orderer_setting_item
        super().__init__(*args, **kwargs)

    def dismiss(self, save=True):
        if save:
            # dismiss popup and save menu bar button order
            self.screen_orderer_setting_item.value = list(
                    reversed(self.screen_orderer.screen_order))
        super().dismiss()

    def open(self):
        from .. import __ALL_SCREENS__
        from ... import __ABISUITE_AVAILABLE__
        from ...app_settings import __MENU_BAR_BUTTON_HEIGHT__, __POPUP_SIZE__
        n_scr = len(__ALL_SCREENS__)
        if not __ABISUITE_AVAILABLE__:
            n_scr -= 1
        super().open(
                "Choose screen ordering",
                size=(__POPUP_SIZE__[0],
                      (n_scr + 3) * __MENU_BAR_BUTTON_HEIGHT__))


class SettingScreenOrderer(SettingItem):
    """Custom setting type to set order of screens.
    """
    def __init__(self, *args, **kwargs):
        self.register_event_type("on_release")
        super().__init__(*args, **kwargs)
        self.button = Button(
                text="Order", font_size="15sp")
        self.button.bind(on_press=self.on_button_pressed)
        self.add_widget(self.button)

    def on_button_pressed(self, instance):
        # open the screen orderer popup.
        orderer = ScreenOrderingPopup(self)
        orderer.open()
