# from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen
from kivy.uix.settings import SettingsWithTabbedPanel  # , SettingItem


class SettingsScreen(Screen):
    """The settings screen which contains the settings panel.
    """
    pass


class AbiControlCenterSettingsScreen(SettingsWithTabbedPanel):
    """The actual settings panel class.
    """
    pass


# # taken from:
# # https://github.com/kivy/kivy/wiki/Buttons-in-Settings-panel
# class SettingsButton(SettingItem):
#     """Button item class for the settings screen.
#     """
#
#     def __init__(self, *args, **kwargs):
#         self.register_event_type("on_release")
#         # don't pass button to super class
#         btn = kwargs.pop("button", None)
#         super().__init__(*args, **kwargs)
#         if btn is None:
#             raise ValueError("Need 'button' data in settings.")
#         button = Button(text=btn["title"], font_size="15sp")
#         button.name = btn["id"]
#         self.add_widget(button)
#         button.bind(on_press=self.on_button_pressed)
#         button = Button(text=btn["title"], font_size="15sp")
#
#     def set_value(self, section, key, value):
#         pass
#
#     def on_button_pressed(self, instance):
#         # self.panel.settings.dispatch(
#         #         "on_config_change", self.panel.config, self.section,
#         #         self.key,
#         #         instance.ID)
#         if instance.name == "update_button":
#             # update the app
#             instance.state = "down"
#             prev_text = instance.text
#             instance.text = "Updating..."
#             update_app()
#             instance.text = prev_text
#             instance.state = "normal"
#         elif instance.name == "check_update_button":
#             # check if an update is available
#             update_avail = update_available()
#             # propagate update info
#             print(update_avail)
#         else:
#             raise TypeError(instance.ID)
