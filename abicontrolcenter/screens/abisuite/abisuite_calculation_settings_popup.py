from ...bases import BasePopup


class AbisuiteCalculationSettingsPopup(BasePopup):
    """Popup class for one calculation settings.
    """
    def __init__(self, abisuite_monitored_calculations, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.abisuite_monitored_calculations = abisuite_monitored_calculations

    def open(self):
        super().open("Calculation Settings")


class AbisuiteNoCalculationSelectedPopup(BasePopup):
    """Popup class when no calculation is selected.
    """
    def open(self):
        super().open("Warning!")
