import abc
import multiprocessing
import subprocess

from kivy.clock import Clock
from kivy.logger import Logger

from ...app_colors import __BLUE__, __ORANGE__
from ...bases import BasePopup


# exitcodes for download_remote function
# NOTE: all exitodes should be unique
__OTHER_EXITCODE__ = 1
__ABIDB_ERROR_EXITCODE__ = 4
__NO_SSH_CONNECTION_EXITCODE__ = 5


class SubProcess:
    """Subprocess class to help handling any type of subprocesses.
    """
    def __init__(self, library, background, hostname, selection):
        """SubProcess init method.

        Parameters
        ----------
        library: str, {'multiprocessing', 'subprocess'}
            The library to use for the subprocess.
        background: bool
            If True, process will be used in background.
        hostname: str
            The hostname for the process.
        selection: list-like
            The list of selected calculations.
        """
        if library not in ("multiprocessing", "subprocess"):
            raise ValueError(library)
        self.library = library
        if background is not True and background is not False:
            raise TypeError(background)
        self.background = background
        self.selection = selection
        self.hostname = hostname
        self.process = None
        self._exitcode = None

    @property
    def exitcode(self):
        if self._exitcode is not None:
            return self._exitcode
        if self.process is None:
            raise RuntimeError("Process not launched.")
        if self.library == "subprocess":
            self._exitcode = self.process.poll()
            return self.exitcode
        elif self.library == "multiprocessing":
            return self.process.exitcode

    @property
    def is_alive(self):
        """Return True if the process is still running.
        """
        if self.process is None:
            raise RuntimeError("Process not launched.")
        if self.library == "subprocess":
            return self.process.poll() is None
        else:
            return self.process.is_alive()

    def kill(self):
        """Kills the process.
        """
        if self.process is None:
            raise RuntimeError("Process not launched.")
        if hasattr(self.process, "kill"):
            self.process.kill()
            return
        if hasattr(self.process, "terminate"):
            self.process.terminate()
            return
        raise RuntimeError(f"Cannot kill process: '{self.hostname}'.")

    def launch_process(self, cmd):
        """Launches the subprocess.

        Parameters
        ----------
        cmd: list of str or func
            If the library used is subprocess, the cmd arg corresponds to
            the actual command line to execute in the process.
            If the library used is multiprocessing, the cmd is the actual
            function to call.
        """
        if self.library == "subprocess":
            from abisuite.routines import is_list_like
            if not is_list_like(cmd):
                cmd = [cmd]
            if not all([isinstance(x, str) for x in cmd]):
                raise ValueError(
                        f"cmd must be a list of str. Got: '{cmd}'.")
            from abisuite.linux_tools import which
            if which(cmd[0]) is None:
                raise FileNotFoundError(cmd)
            if self.background:
                self.process = subprocess.Popen(cmd)
            else:
                self.process = subprocess.run(cmd)
        elif self.library == "multiprocessing":
            if not callable(cmd):
                raise TypeError(f"'cmd' must be callable. Got '{cmd}'.")
            self.process = multiprocessing.Process(
                    name=self.hostname,
                    target=cmd,
                    args=(self.hostname, ))
            self.process.daemon = True
            self.process.start()
            if not self.background:
                self.process.join()


class BaseAbisuiteSubProcessor(abc.ABC):
    """Base class for an abisuite subprocessor. Subclasses will
    use the abisuite capabilities
    """
    script = None
    script_args = None

    def __init__(self,
                 monitored_calculations,
                 refresh_rate=1,
                 ):
        """Subprocessor init method.

        Parameters
        ----------
        monitored_calculations: AbisuiteMonitoredCalculations object
        refresh_rate: float, int, optional
            The refresh rate in seconds to look out for subprocesses when
            they are launched.
        """
        if self.script is None:
            raise ValueError("Need to set script.")
        if self.script_args is not None:
            # should be a tuple
            if not isinstance(self.script_args, tuple):
                raise TypeError("'script_args' should be a tuple.")
        self.monitored_calculations = monitored_calculations
        self.subprocesses = None  # pool for the subprocesses
        # dict containing (un)completed subprocesses selections
        # key = hostnames, values = list of selected items
        # useful for monitoring
        self.uncompleted_subprocesses = {}
        self.completed_subprocesses = {}
        Clock.schedule_interval(self.check_status, refresh_rate)

    @abc.abstractmethod
    def all_process_ended_callback(self):
        pass

    def automatic_refresh(self, *args):
        """Perform an automatic forced refresh.
        """
        self.monitored_calculations.refresh(_automatic=True)

    def check_status(self, dt):
        """Checks the status of subprocesses.
        """
        if self.subprocesses is None:
            # nothing to check
            return
        all_finished = True
        for hostname, process in self.subprocesses.items():
            # depending on the type of subprocess, we need to check differently
            if process.is_alive:
                all_finished = False
                continue
            # if we are here, the process is over
            if hostname not in self.completed_subprocesses:
                self.completed_subprocesses[hostname] = process
                # remove from uncompleted processes
                self.uncompleted_subprocesses.pop(hostname)
                # call the callback (this is done only once)
                self.process_ended_callback(hostname, process.exitcode)
        # all subprocesses ended, call the callback if needed.
        if all_finished:
            # callback after all processes have ended
            exitcodes = [
                    process.exitcode for process in self.subprocesses.values()]
            self.subprocesses = None
            self.all_process_ended_callback(exitcodes)

    def kill_processes(self):
        """Kill processes if any are still running.
        """
        if self.subprocesses is None:
            # nothing to do
            return
        Logger.info("Killing abisuite live processes that remains.")
        for process in self.subprocesses.values():
            process.kill()
        self.subprocesses = None
        self.uncompleted_subprocesses = {}
        self.completed_subprocesses = {}

    def launch_script(self, selected_items, background=True):
        """Launch the script which is subprocessed.

        Parameters
        ----------
        selected_items: dict
            Dictionary containing as keys the hostnames of the selected
            calculations and as values, the list of the calculations to
            launch.
        background: bool, optional
            If True, the task will be launched in the background (default)
            as a subprocess. Otherwise it is run in the foreground and
            everything waits for it to finish. Useful for unittests.
        """
        if self.subprocesses is not None:
            # active processes are still in route. don't relaunch them.
            Logger.warning(
                f"{self.__class__}:Some processes are still running! "
                "Nothing done yet")
            return
        self.subprocesses = {}
        if background:
            self.uncompleted_subprocesses = {}
            self.completed_subprocesses = {}
        for hostname, selection in selected_items.items():
            if isinstance(self.script, str):
                process = SubProcess(
                        "subprocess", background, hostname, selection)
                # we call an external script
                # => use subprocess.Popen
                cmd = [self.script]
                if hostname != "local":
                    cmd += [f"--remote={hostname}"]
                cmd += list(selection)
                # add script args if not None
                if self.script_args is not None:
                    cmd += list(self.script_args)
            elif callable(self.script):
                # we call an internal function
                # => use multiprocessing.Process
                process = SubProcess(
                        "multiprocessing", background, hostname, selection)
                cmd = self.__class__.script
            # launch the process
            process.launch_process(cmd)
            # add the process to the list of running processes
            self.subprocesses[hostname] = process
            if background:
                self.uncompleted_subprocesses[hostname] = process
                # a subprocess has been created, call the callback
                # this is done only once per process
                self.process_started_callback(hostname, process.selection)
            else:
                # the process will have finished already
                # already call the process_ended_callback
                self.process_ended_callback(hostname, process.exitcode)
        if not background:
            # already call the all_process_ended_callback
            exitcodes = [
                    process.exitcode for process in self.subprocesses.values()]
            self.subprocesses = None
            self.all_process_ended_callback(exitcodes)

    @abc.abstractmethod
    def process_ended_callback(self):
        pass

    @abc.abstractmethod
    def process_started_callback(self):
        pass

    def set_selected_status_label_text(
            self, text, hostname, selection, color=None):
        """Sets all the status labels of the selection to a given text.

        Parameters
        ----------
        text: str
            The status label text to set to.
        hostname: str
            The hostname to change.
        selection: list
            The list of selected items.
        color: list_like, optional
            If color is not None, it will be the new text color.
        """
        for calc in selection:
            layout = self.monitored_calculations.get_calculation_line_layout(
                    hostname, calc)
            layout.status_label.text = "[b]" + text + "[/b]"
            if color is not None:
                layout.status_label.color = color

    def replace_selected_status_label_text(
            self, to_replace, replace_with, hostname):
        """Replace the text of all the labels of a hostname by another
        given string.

        Parameters
        ----------
        to_replace: str
            The text to replace.
        replace_with: str
            The text that will replace.
        hostname: str
            The hostname to act on.
        """
        for layout in self.monitored_calculations.calculation_lines[hostname]:
            if to_replace in layout.status_label.text:
                # replace thie keyword for 'Deleted'
                layout.status_label.text = layout.status_label.text.replace(
                        to_replace, replace_with)


class AbidbStopMonitoringSubProcessor(BaseAbisuiteSubProcessor):
    """The Subprocessor handling stop monitoring action.

    When this is launched, the abidb script is used (include remote operations)
    to stop monitoring calculations.
    """
    script = "abidb"
    script_args = ("--set-monitored-false", )

    def all_process_ended_callback(self, *args, **kwargs):
        self.automatic_refresh()

    def process_ended_callback(self, hostname, *args):
        """One abidb process have ended. Change the corresponding label text.
        """
        # reset everything since we need to recompute all the heights and label
        # positions
        self.monitored_calculations.add_locally_available_remotes()

    def process_started_callback(self, *args):
        """Sets the calculation statuses to 'deleting'.
        """
        self.set_selected_status_label_text(
                "Stopping to monitor", *args, color=__ORANGE__)


def download_remote_database(remote):
    """Download remote database.

    Parameters
    ----------
    remote: str
        Remote name to download.

    Returns
    -------
    bool: True if we were able to download the database. False if no connection
          could be established.
    """
    import sys

    from paramiko.ssh_exception import NoValidConnectionsError

    from abicontrolcenter.screens.abisuite import (
            __ABIDB_ERROR_EXITCODE__,
            __NO_SSH_CONNECTION_EXITCODE__,
            __OTHER_EXITCODE__,
            )
    from abisuite.databases import AbiDB
    from abisuite.databases.exceptions import AbiDBRuntimeError
    if remote == "local":
        return
    try:
        AbiDB.from_remote(remote, download=True, refresh=True)
        return True
    except AbiDBRuntimeError:
        # something happened on remote server while getting db infos
        sys.exit(__ABIDB_ERROR_EXITCODE__)
    except (NoValidConnectionsError, TimeoutError):
        # could't establish connection
        sys.exit(__NO_SSH_CONNECTION_EXITCODE__)
    except Exception:
        sys.exit(__OTHER_EXITCODE__)


class AbiRefreshSubProcessor(BaseAbisuiteSubProcessor):
    """The subprocessor handling the refresh of the abisuite screen.
    """
    script = download_remote_database

    def all_process_ended_callback(self, *args):
        """Resets the last_refresh timestamp. And reset the refresh button
        text.
        """
        self.monitored_calculations.reset_refresh_timestamp()
        self.monitored_calculations.refresh_button.state = "normal"
        self.monitored_calculations.refresh_button.text = "Refresh"
        Logger.info("Refreshing: Done!")

    def add_refreshing_tags(self, hostname, selection):
        """Adds 'refreshing' tags to all remotes that are being refreshed.
        Also adds this kind of tags to calculations that are not completed yet.
        """
        # get remote label
        remote_line = self.monitored_calculations.remote_lines[hostname]
        if remote_line is None:
            # this remote line was not shown, thus don't need to change
            return
        remote_label = remote_line.hostname_label
        # change its text
        remote_label.text += " (Refreshing...)"
        # change calculation lines text if they are not completed
        for calc in selection:
            layout = self.monitored_calculations.get_calculation_line_layout(
                    hostname, calc)
            text = layout.status_label.text
            # continue if calculation is finished
            if "Error" in text:
                continue
            if "Completed" in text:
                continue
            layout.status_label.text += " (Refreshing...)"

    def launch_script(self, *args, _automatic=False, **kwargs):
        """override this method since we launch refresh for all remotes.

        Don't need to pass 'selected' items to mother method.

        Parameters
        ----------
        _automatic: bool, optional
            If True, the refresh button text will contain the 'automatic' tag.
        """
        mc = self.monitored_calculations  # shortcut
        # _automatic is a flag to tell if the refresh is done automatically
        automatic = ""
        if _automatic:
            automatic = "Automatically "
        Logger.info(f"{automatic}Refreshing: abisuite screen...")
        if _automatic:
            mc.refresh_button.text = "Automatically Refreshin'..."
        else:
            mc.refresh_button.text = "Refreshin'..."
        mc.refresh_button.state = "down"
        # dictionary where all keys are the remotes and selected values
        # are all the locally available monitored calculations
        selected = {remote["hostname"]: mc.get_monitored_calculations(
            remote["abidb"])[0]
                    for remote in mc.databases.values()}
        super().launch_script(selected, *args, **kwargs)

    def process_ended_callback(self, hostname, exitcode):
        """Remove the refreshing tag of the remote label and reset the screen.
        """
        if exitcode:
            # exitcode is non-zero. something bad happened during the
            # subprocess.
            self.monitored_calculations.databases[hostname]["online"] = False
        else:
            # everything went well, set online to true
            self.monitored_calculations.databases[hostname]["online"] = True
        # reset everything since we need to recompute all the heights and label
        # positions
        self.monitored_calculations.add_locally_available_remotes()
        # readd the 'refreshing' text to all the refreshing processes
        for host, process in self.uncompleted_subprocesses.items():
            self.add_refreshing_tags(host, process.selection)

    def process_started_callback(self, hostname, selection):
        """Add a refreshing tag to remotes being refreshed.

        Also add refreshing tag for each calculations that are not finished.
        """
        self.add_refreshing_tags(hostname, selection)


class AbirmSubProcessor(BaseAbisuiteSubProcessor):
    """The Subprocessor handling the abirm script.
    """
    script = "abirm"

    def all_process_ended_callback(self, *args, **kwargs):
        self.automatic_refresh()

    def process_ended_callback(self, hostname, *args):
        """One abirm process have ended. Change the corresponding label text.
        """
        self.replace_selected_status_label_text(
                "Deleting", "Deleted", hostname)

    def process_started_callback(self, *args):
        """Sets the calculation statuses to 'deleting'.
        """
        self.set_selected_status_label_text(
                "Deleting", *args, color=__ORANGE__)


class AbiRelaunchSubProcessor(BaseAbisuiteSubProcessor):
    """The subprocessor handling the processes of the abirelaunch script.
    """
    script = "abirelaunch"

    def all_process_ended_callback(self, *args, **kwargs):
        self.automatic_refresh()

    def process_ended_callback(self, hostname, *args):
        """One abirelaunch process have ended. Change the corresponding label text.
        """
        self.replace_selected_status_label_text(
                "Relaunching", "Relaunched", hostname)

    def process_started_callback(self, *args):
        """Sets the calculation statuses to 'Relaunching'.
        """
        self.set_selected_status_label_text(
                "Relaunching", *args, color=__BLUE__)


class SubprocessorController:
    """Container for all the subprocessors. It controls how to use the
    different subprocessors.
    """
    def __init__(self, abisuite_monitored_calculations):
        self.subprocessors = {}
        self.subprocessors["delete"] = AbirmSubProcessor(
                abisuite_monitored_calculations)
        stop = AbidbStopMonitoringSubProcessor(
                abisuite_monitored_calculations)
        self.subprocessors["stop_monitoring"] = stop
        self.subprocessors["refresh"] = AbiRefreshSubProcessor(
                abisuite_monitored_calculations)
        self.subprocessors["relaunch"] = AbiRelaunchSubProcessor(
                abisuite_monitored_calculations)

    def kill_remaining_processes(self):
        """Kills the processes still running.
        """
        for task_name, subprocessor in self.subprocessors.items():
            if subprocessor.subprocesses is not None:
                Logger.info(f"Killing remaining '{task_name}' processes.")
                subprocessor.kill_processes()

    def launch_subprocesses(self, task_name, *args, **kwargs):
        """Launches a subprocessor.

        Checks that no subprocesses are running still. Otherwise
        a popup is shown to wait for tasks to complete.

        Parameters
        ----------
        task_name: str
            The task name to execute.
        """
        # check that task is valid
        if task_name not in self.subprocessors:
            raise NotImplementedError(task_name)
        # check that no other task are still running
        Logger.info(f"SubprocessorController: task requested '{task_name}'.")
        for task, subprocessor in self.subprocessors.items():
            if subprocessor.subprocesses is not None:
                # task are still running
                Logger.info(
                        "SubprocessorController: task already running "
                        f"'{task}'.")
                self.raise_task_already_running_popup(task)
                return
        # no tasks are still running, launch subprocesses
        Logger.info(f"SubprocessorController: launching '{task_name}'.")
        self.subprocessors[task_name].launch_script(*args, **kwargs)

    def raise_task_already_running_popup(self, task):
        """Open a popup to tell user that a task is still running in the
        background.
        """
        AlreadyRunningTaskPopup(task).open()


class AlreadyRunningTaskPopup(BasePopup):
    """Popup class that warns user that a calculation is already running.
    """
    def __init__(self, task, *args, **kwargs):
        self.task_running = task
        super().__init__(*args, **kwargs)

    def open(self):
        super().open("Cannot execute task!")
