import datetime
import os
import sqlite3

from kivy.app import App
from kivy.clock import Clock
from kivy.logger import Logger
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.checkbox import CheckBox
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.behaviors.button import ButtonBehavior

from . import __ABISUITE_REFRESH_FREQS_OPTIONS__
from .abisuite_calculation_settings_popup import (
        AbisuiteCalculationSettingsPopup,
        AbisuiteNoCalculationSelectedPopup,
        )
from .abisuite_subprocessors import SubprocessorController
from ...app_colors import __WHITE__, __BLUE__, __GREEN__, __RED__, __TEAL__
from ...app_settings import (
       __MENU_BAR_BUTTON_HEIGHT__,
       )
from ...custom_widgets import (
        BackgroundColor, ContouredCheckBox, ContouredLabel,  # HoverBehavior,
        )
from ..._paths import __ABICC_ICON_DIR__
from ...routines import get_user_config


__REMOTE_CONTOUR_WIDTH__ = 1.5
__CALCULATION_LABEL_HEIGHT__ = 20
__NO_CONNECTION_ICON__ = os.path.join(__ABICC_ICON_DIR__, "no_connection.png")
__MAX_CALCULATIONS_TO_SHOW__ = 100


class MonitoredCalculationLine(ButtonBehavior, RelativeLayout):
    # HoverBehavior):
    """Custom class for the monitored calculations layout line.
    """
    calculation = StringProperty(None)
    project = StringProperty(None)
    checkbox = ObjectProperty(None)


class MonitoredCalculationLabel(Label, BackgroundColor):
    """Monitored calculation label class.
    """
    monitored_calculation_layout = ObjectProperty(None)

    def __init__(self, *args, **kwargs):
        Label.__init__(self, *args, **kwargs)
        BackgroundColor.__init__(self)


class MonitoredProjectLabel(Label, BackgroundColor):
    """The project label of a given calculation.
    """
    monitored_calculation_layout = ObjectProperty(None)
    project = StringProperty(None)

    def __init__(self, *args, **kwargs):
        Label.__init__(self, *args, **kwargs)
        BackgroundColor.__init__(self)


class MonitoredCalculationCheckBox(CheckBox, BackgroundColor):
    """Monitored calculation checkbox class.
    """
    monitored_calculation_layout = ObjectProperty(None)

    def __init__(self, *args, **kwargs):
        CheckBox.__init__(self, *args, **kwargs)
        BackgroundColor.__init__(self)


class RemoteLine(RelativeLayout):
    """Layout that holds the remote label and a global checkbox.
    """
    monitored_calculation_layout = ObjectProperty(None)
    hostname = StringProperty(None)
    hostname_label = ObjectProperty(None)
    checkbox = ObjectProperty(None)

    def __init__(self, *args, online=True, **kwargs):
        """Init method for the RemoteLine class.

        Parameters
        ----------
        online: bool, optional
            If False, an icon is added to show that the remote is not
            reachable. The contour colors are changed as well.
        """
        super().__init__(*args, **kwargs)
        if online:
            color = __WHITE__
        else:
            color = __RED__
        # add checkbox first
        if online:
            # add checkbox
            self.checkbox = self.get_checkbox(color)
            self.checkbox.hostname = self.hostname
            self.checkbox.bind(
                    active=self.monitored_calculation_layout.global_selection)
            self.add_widget(self.checkbox)
        else:
            # add no connection icon instead
            self.no_connection_icon = self.get_no_connection_icon()
            self.add_widget(self.no_connection_icon)
        # add hostname label
        self.hostname_label = self.get_hostname_label(color)
        self.hostname_label.bind(size=self.hostname_label.setter("text_size"))
        self.add_widget(self.hostname_label)

    def get_checkbox(self, color):
        """Returns the checkbox widget.

        Parameters
        ----------
        color: list
            The contour color of the checkbox.
        """
        return ContouredCheckBox(
                    contour_top=True,
                    contour_width=__REMOTE_CONTOUR_WIDTH__,
                    pos_hint={"x": 0, "y": 0},
                    size_hint=(0.05, 1),
                    contour_color=color,
                    )

    def get_hostname_label(self, color):
        """Returns the hostname label.

        Parameters
        ----------
        color: list
            The contour color.
        """
        return ContouredLabel(
                    text=self.hostname,
                    font_size=25,
                    halign="left",
                    pos_hint={"x": 0.05, "y": 0},
                    # x size = 1 because ContouredCheckBox does not work...
                    size_hint=(1, 1),
                    contour_top=True,
                    contour_width=__REMOTE_CONTOUR_WIDTH__,
                    contour_color=color,
                    )

    def get_no_connection_icon(self):
        """Returns the no connection icon widget.
        """
        return Image(
                source=__NO_CONNECTION_ICON__,
                pos_hint={"x": 0, "y": 0},
                size_hint=(0.05, 1),
                anim_loop=0,  # infinite loop (gif)
                )


class AbisuiteMonitoredCalculations(StackLayout):
    """The actual abisuite screen that shows the monitored calculations.

    All this is in python since it is dynamically updated.
    """
    abisuite_screen_buttons = ObjectProperty(None)
    no_monitored_calculations_label = ObjectProperty(None)
    _automatic_refresh_dt = 120  # seconds (2 mins)
    _subprocess_refresh_rate = 1  # seconds

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._last_refresh = datetime.datetime.now()
        self._clock = Clock.schedule_interval(
                self.automatic_refresh, self._automatic_refresh_dt)
        self._show_unconnected_remotes = None
        self._show_empty_remotes = None
        self._allow_remotes = None
        self.calculation_lines = {}
        self.remote_lines = {}
        self._databases = None
        # subprocessors that handles tasks done by abisuite scripts
        self.subprocessors = SubprocessorController(self)
        # self.add_locally_available_remotes()
        # self.bind(minimum_height=self.setter("height"))

    @property
    def allow_remotes(self):
        if self._allow_remotes is not None:
            return self._allow_remotes
        if App.get_running_app() is not None:
            self._allow_remotes = App.get_running_app().allow_remotes
            return self.allow_remotes
        else:
            raise ValueError("Need to set 'allow_remotes' manually.")

    @allow_remotes.setter
    def allow_remotes(self, allow_remotes):
        # manual setting
        self._allow_remotes = allow_remotes

    @property
    def databases(self):
        if self._databases is not None:
            return self._databases
        self._databases = self.get_databases()
        return self.databases

    @property
    def n_selected(self):
        """Number of selected calculations.
        """
        return sum([len(x) for x in self.selected.values()])

    @property
    def remotes(self):
        """The list of remotes.
        """
        if not self.allow_remotes:
            return []
        from abisuite import USER_CONFIG as ABISUITE_USER_CONFIG
        return ABISUITE_USER_CONFIG.REMOTES.remotes

    @property
    def refresh_button(self):
        """Shortcut to the refresh_button stored in the AbisuiteControlBar.
        """
        return self.abisuite_screen_buttons.refresh_button

    @property
    def selected(self):
        return self.get_selected_calculations()

    @property
    def show_empty_remotes(self):
        if self._show_empty_remotes is not None:
            return self._show_empty_remotes
        # set from config if app exists
        if App.get_running_app() is not None:
            # read config
            self._show_empty_remotes = get_user_config(
                         "abisuite.appearance", "show_empty_remotes",
                         dtype=bool)
            return self._show_empty_remotes
        raise ValueError("Need to set 'show_empty_remotes' manually.")

    @show_empty_remotes.setter
    def show_empty_remotes(self, show):
        if show is not True and show is not False and show is not None:
            raise TypeError(show)
        self._show_empty_remotes = show

    @property
    def show_unconnected_remotes(self):
        if self._show_unconnected_remotes is not None:
            return self._show_unconnected_remotes
        if App.get_running_app() is not None:
            self._show_unconnected_remotes = get_user_config(
                    "abisuite.appearance", "show_unconnected_remotes",
                    dtype=bool,
                    )
            return self.show_unconnected_remotes
        raise ValueError("Need to set 'show_unconnected_remotes' manually.")

    @show_unconnected_remotes.setter
    def show_unconnected_remotes(self, show):
        if show is not True and show is not False and show is not None:
            raise TypeError(show)
        self._show_unconnected_remotes = show

    def add_locally_available_remotes(self):
        """Clear widgets then adds the locally available remotes lines with
        their calculation lines stored locally.
        """
        # get selected items if any to keep them selected after the reset
        selected_calculations = self.selected.copy()
        selected_remote_lines = self.get_selected_remote_lines()
        self.clear_widgets()
        del self.calculation_lines
        self.calculation_lines = {}
        del self.remote_lines
        self.remote_lines = {}
        for db in self.databases.values():
            remote = db["hostname"]
            abidb = db["abidb"]
            # skip if we don't show unconnected remotes
            # refresh monitored calculation statuses. this is only a local
            # operation
            self.refresh_monitored(abidb)
            calculations, projects = self.get_monitored_calculations(abidb)
            if not calculations and not self.show_empty_remotes:
                # don't show empty remotes if user don't want to
                # no calculations to show and don't show it's empty
                self.remote_lines[remote] = None
                continue
            if not db["online"] and not self.show_unconnected_remotes:
                self.remote_lines[remote] = None
                continue
            # add remote label
            self._add_remote_label(remote, online=db["online"])
            # check the checkbox if it was already checked before reset
            if remote in selected_remote_lines:
                self.remote_lines[remote].checkbox.active = True
            if not calculations:
                # if no calculations to show, add label to tell this
                self._add_no_calculations_label(online=db["online"])
                self.calculation_lines[remote] = []  # empty list
                continue
            # add the locally available calculations
            self._add_monitored_calculations(abidb, calculations, projects)
            # check the relevant checkboxes if they were already selected
            # before the refresh
            if remote not in selected_calculations:
                continue
            selected = selected_calculations[remote]
            for selec in selected:
                if selec not in calculations:
                    # this calculation disappeared
                    continue
                # check this checkbox because it was already selected before
                layout = self.get_calculation_line_layout(remote, selec)
                layout.checkbox.active = True

    def automatic_refresh(self, *args, **kwargs):
        """Performs an 'automatic refresh'.

        Basically, this method is called once upon a time
        (every self._automatic_refresh_dt seconds) or if an update to the
        corresponding settings is made. If this is called, it will do a refresh
        will be done if the time since the last refresh exceeds a given
        threshold (given by the settings). Otherwise it waits until next
        _automatic_refresh_dt.
        """
        # get total time to wait from settings
        wait_option = get_user_config(
                "abisuite.refresh", "automatic_refresh_frequency")
        wait_time = __ABISUITE_REFRESH_FREQS_OPTIONS__[wait_option]  # seconds
        delta = datetime.datetime.now() - self._last_refresh
        if delta.total_seconds() <= wait_time:
            return
        # time to do a refresh
        # change button refresh type to 'automatic refresh'
        self.refresh_button.refresh_type = "automatic"
        # actually perform refresh
        # refresh() will update the _last_refresh attribute
        self.refresh_button.trigger_action()
        # reset button to normal state
        self.refresh_button.refresh_type = "manual"

    def delete_calculations(self):
        """Delete the selected calculations.
        """
        self.subprocessors.launch_subprocesses("delete", self.selected)

    def get_calculation_line_layout(self, hostname, calculation):
        """Returns the layout for a given hostname and calculation.

        Parameters
        ----------
        hostname: str
            The hostname to look for.
        calculation: str
            The calculation path to look for.

        Returns
        -------
        The MonitoredCalculationLine layout for this calculation.

        Raises
        ------
        LookupError: if cannot find the given calculation.
        """
        if hostname not in self.calculation_lines:
            self.add_locally_available_remotes()
        for layout in self.calculation_lines[hostname]:
            if layout.calculation == calculation:
                return layout
        raise LookupError(
                f"Cannot find calculation '{calculation}' for remote "
                f"'{hostname}'.")

    def get_databases(self):
        """Returns the list of databases data. Each item are dicts containing
        the hostname, the abidb in itself and an online tag.
        """
        from abisuite import USER_CONFIG as ABISUITE_USER_CONFIG
        from abisuite.databases import AbiDB
        from abisuite.databases.exceptions import CachedAbiDBFileNotFoundError
        # create link with databases
        # start with the local one
        databases = {"local": {"hostname": "local",
                     "abidb": AbiDB.from_file(
                          ABISUITE_USER_CONFIG.DATABASE.path),
                     "online": True}}
        # local db is always online
        if not self.allow_remotes:
            # app doesn't allow remotes (maybe we are in a test)
            return databases
        # add remote databases
        for remote in self.remotes:
            db_data = {
                    "hostname": remote["hostname"],
                    "online": True,
                    }
            try:
                db_data["abidb"] = AbiDB.from_remote(
                                remote["hostname"], download=False)
            except CachedAbiDBFileNotFoundError:
                # this cached file does not exist yet
                # create a temporary one that will be downloaded anyway later
                db = AbiDB(remote_data=remote)
                db.create_database(
                        AbiDB.get_cached_db_path(remote["hostname"]))
                db_data["abidb"] = db
                db_data["online"] = False
            databases[db_data["hostname"]] = db_data
        return databases

    def get_monitored_calculations(self, abidb):
        """Returns the list of paths of the monitored calculations.

        Parameters
        ----------
        abidb : AbiDB object
            The database object which we extract the calculations.

        Returns
        -------
        tuple: a length 2 tuple whose first element is the list of
               monitored calculations and the second is the list
               of corresponding projects.
        """
        try:
            calcs = abidb.get_all_monitored_calculations()
            projects = [abidb.get_project(calc) for calc in calcs]
            if len(calcs) > __MAX_CALCULATIONS_TO_SHOW__:
                calcs = calcs[:__MAX_CALCULATIONS_TO_SHOW__]
                projects = projects[:__MAX_CALCULATIONS_TO_SHOW__]
            return (calcs, projects)
        except sqlite3.DatabaseError as e:
            Logger.error(
                    "AbisuiteMonitoredCalculations|get_monitored_calculations:"
                    f" {e}")
            return [[], []]

    def get_calculation_status(self, abidb, calculation):
        """Returns the status of a calculation.

        Parameters
        ----------
        abidb: AbiDB object
            The database object from which we get the data.
        calculation: str
            The path of a calculation.
        """
        try:
            return abidb.get_status(calculation)
        except sqlite3.DatabaseError as e:
            Logger.error(f"AbisuiteMonitoredCalculations: {e}")
            return "ERROR"

    def get_selected_calculations(self):
        """Returns the list of selected calculations.
        """
        select = {}
        for hostname, layouts in self.calculation_lines.items():
            select.setdefault(hostname, [])
            for layout in layouts:
                if layout.checkbox.active:
                    select[hostname].append(layout.calculation)
        return select

    def get_selected_remote_lines(self):
        """Returns the list of selected remote lines.
        """
        selected = []
        for hostname, remote_line in self.remote_lines.items():
            if remote_line is None:
                # this remote line was not shown, thus could'nt possibly be
                # checked
                continue
            if remote_line.checkbox is None:
                continue
            if remote_line.checkbox.active:
                selected.append(hostname)
        return selected

    def open_calculation_settings_popup(self):
        """Opens the calculation settings popup.
        """
        if not self.n_selected:
            # no calculation selected
            # open the 'please select a calculation' popup instead
            self.open_no_calculation_selected_popup()
            return
        popup = AbisuiteCalculationSettingsPopup(self)
        popup.open()

    def global_selection(self, checkbox, value):
        """Activate a global selection from a remote global checkbox.
        """
        hostname = checkbox.hostname
        if hostname not in self.calculation_lines:
            # no calculations shown
            return
        for calculation_line in self.calculation_lines[hostname]:
            calculation_line.checkbox.active = value

    def kill_processes(self):
        """On exit, if there are still running processes, kill them.
        """
        self.subprocessors.kill_remaining_processes()

    def open_no_calculation_selected_popup(self):
        """Opens a popup that says no calculations are selected.
        """
        popup = AbisuiteNoCalculationSelectedPopup()
        popup.open()

    def refresh(self, **kwargs):
        """Builds the layout of the monitored calculations.
        """
        self.subprocessors.launch_subprocesses("refresh", **kwargs)

    def refresh_monitored(self, abidb):
        """Refreshes the 'monitored' attribute of an AbiDB object.
        """
        try:
            abidb.refresh_monitored()
        except sqlite3.DatabaseError as e:
            Logger.error(f"AbisuiteMonitoredCalculations: {e}")

    def relaunch_calculations(self):
        """Relaunch the selected calculations.
        """
        self.subprocessors.launch_subprocesses(
                "relaunch", self.selected)

    def reset_refresh_timestamp(self):
        """Resets the refresh timestamp.
        """
        self._last_refresh = datetime.datetime.now()

    def stop_monitoring(self):
        """Stop monitoring the selected calculations.
        """
        self.subprocessors.launch_subprocesses(
                "stop_monitoring", self.selected)

    def _add_remote_label(self, remote, **kwargs):
        # create and add a label that tags the remote machine in the list
        remote_line = RemoteLine(
                hostname=remote,
                monitored_calculation_layout=self,
                size_hint=(1, None),
                height=__MENU_BAR_BUTTON_HEIGHT__,
                **kwargs,
                )
        self.add_widget(remote_line)
        self.remote_lines[remote] = remote_line

    def _add_monitored_calculations(self, abidb, calculations, projects):
        # calculations: list containing the paths of the calculations
        # projects: list of projects associated with the calculations
        for calculation, project in zip(calculations, projects):
            # checkbox for selection
            layout = MonitoredCalculationLine(
                        size_hint=(1, None),
                        height=__CALCULATION_LABEL_HEIGHT__,
                        calculation=calculation,
                        project=project)
            checkbox = MonitoredCalculationCheckBox(
                    monitored_calculation_layout=layout,
                    size_hint=(0.05, 1),
                    pos_hint={"x": 0, "y": 0},
                    )
            layout.checkbox = checkbox
            path_label = MonitoredCalculationLabel(
                    monitored_calculation_layout=layout,
                    text=calculation,
                    size_hint=(2/3, 1),
                    pos_hint={"x": checkbox.size_hint[0], "y": 0},
                    # height=__CALCULATION_LABEL_HEIGHT__,
                    halign="left",
                    valign="center",
                    )
            path_label.bind(size=path_label.setter('text_size'))
            project_label = MonitoredProjectLabel(
                    monitored_calculation_layout=layout,
                    text=project or "",  # project can be None
                    size_hint=(1/6, 1),
                    pos_hint={
                        "x": (
                            path_label.pos_hint["x"] +
                            path_label.size_hint[0]),
                        "y": 0},
                    halign="center",
                    valign="center",
                    )
            project_label.bind(size=project_label.setter("text_size"))
            status = self.get_calculation_status(abidb, calculation)
            if status != "ERROR":
                if status["calculation_started"] is False:
                    status_txt = "Not started"
                    color = __BLUE__
                elif status["calculation_finished"] is True:
                    status_txt = "Completed"
                    color = __GREEN__
                elif status["calculation_finished"] == "error":
                    status_txt = "Error"
                    color = __RED__
                elif status["calculation_finished"] is False:
                    status_txt = "Running"
                    color = __TEAL__
            else:
                status_txt = "DB ERROR (SEE LOG)"
                color = __RED__
            # bold the text
            status_txt = "[b]" + status_txt + "[/b]"
            status_label = MonitoredCalculationLabel(
                    monitored_calculation_layout=layout,
                    color=color,
                    text=status_txt,
                    size_hint=(1/6 - 0.05, 1),
                    pos_hint={
                        "x": (
                            project_label.pos_hint["x"] +
                            project_label.size_hint[0]),
                        "y": 0},
                    # height=__CALCULATION_LABEL_HEIGHT__,
                    markup=True,
                    )
            layout.status_label = status_label
            layout.add_widget(checkbox)
            layout.add_widget(path_label)
            layout.add_widget(project_label)
            layout.add_widget(status_label)
            self.add_widget(layout)
            if abidb.is_remote:
                hostname = abidb.remote_data["hostname"]
            else:
                hostname = "local"
            self.calculation_lines.setdefault(hostname, [])
            self.calculation_lines[hostname].append(layout)

    def _add_no_calculations_label(self, online=True):
        # add a label that says that no calculations are monitored atm
        # this label fills the whole space
        text = "No monitored calculations to show."
        if not online:
            text = "Cannot establish connection with remote..."
        no_monitored_calculations_label = Label(
                text=text,
                size_hint=(1, None),
                height=__CALCULATION_LABEL_HEIGHT__,
                halign="left")
        no_monitored_calculations_label.bind(
                size=no_monitored_calculations_label.setter('text_size'))
        self.add_widget(no_monitored_calculations_label)
