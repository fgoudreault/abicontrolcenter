import json

from .abisuite_screen import AbisuiteScreen
from .abisuite_subprocessors import (
        __ABIDB_ERROR_EXITCODE__,
        __NO_SSH_CONNECTION_EXITCODE__,
        __OTHER_EXITCODE__,
        )


__ABISUITE_MONITORED_CALCULATION_HOVER_COLOR__ = [0, 0, 0.8, 1]
# refresh options in seconds
__ABISUITE_REFRESH_FREQS_OPTIONS__ = {
        "every 5 mins": 300,
        "every 30 mins": 1800,
        "every 1 hour": 3600,
        "every 2 hours": 7200,
        "every 12 hours": 43200,
        }
__ALL_ABISUITE_DATA__ = {
        "kv_files": [
            "abisuite_calculation_settings_popup.kv",
            "abisuite_screen.kv",
            "abisuite_monitored_calculations.kv",
            "abisuite_subprocessors.kv",
            ],
        "screen_cls": AbisuiteScreen,
        "settings": json.dumps([
            {"type": "bool",
             "title": "Show empty remotes",
             "desc": ("If True, hides the remotes when they don't have any "
                      "calculations to show."),
             "key": "show_empty_remotes",
             "section": "abisuite.appearance"},
            {"type": "bool",
             "title": "Show unconnected remotes",
             "desc": ("If True, shows the remotes even if they can't be "
                      "reached via ssh."),
             "key": "show_unconnected_remotes",
             "section": "abisuite.appearance"},
            {"type": "options",
             "title": "Automatic refresh frequency",
             "desc": "Performs an automatic refresh at this frequency.",
             "key": "automatic_refresh_frequency",
             "section": "abisuite.refresh",
             "options": list(__ABISUITE_REFRESH_FREQS_OPTIONS__.keys()),
             },
            ]),
        "default_settings": {
            # abisuite
            "abisuite.appearance": {
                "show_empty_remotes": 1,
                "show_unconnected_remotes": 1,
                },
            "abisuite.refresh": {
                "automatic_refresh_frequency": "every 1 hour",
                }},
            }
