from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.relativelayout import RelativeLayout

from ..bases import BaseScreen


class AbisuiteScreen(BaseScreen):
    """The Abisuite screen object.
    """
    abisuite_screen_content = ObjectProperty(None)

    @property
    def abisuite_monitored_calculations(self):
        return self.abisuite_screen_content.abisuite_monitored_calculations

    def automatic_refresh(self):
        """Performs an 'automatic_refresh'. It will do a real refresh
        if time since last refresh exceeds refresh rate. Otherwise it
        will do nothing.
        """
        self.abisuite_screen_content.automatic_refresh()

    def refresh(self, *args, **kwargs):
        """Refreshes the abisuite screen.
        """
        self.abisuite_screen_content.refresh(*args, **kwargs)


class AbisuiteScreenContent(RelativeLayout):
    """The content of the abisuite screen.

    Made of two parts:
        1 - Buttons
        2 - Monitored calculations
    """
    abisuite_monitored_calculations = ObjectProperty(None)
    abisuite_screen_buttons = ObjectProperty(None)

    @property
    def refresh_button(self):
        return self.abisuite_screen_buttons.refresh_button

    def automatic_refresh(self):
        """Performs an 'automatic_refresh'. It will do a real refresh
        if time since last refresh exceeds refresh rate. Otherwise it
        will do nothing.
        """
        self.abisuite_monitored_calculations.automatic_refresh()

    def refresh(self, *args, **kwargs):
        """Perform a refresh of the abisuite app.
        """
        # directly call the abisuite monitored calculations refresh action
        self.abisuite_monitored_calculations.refresh(*args, **kwargs)


class RefreshButton(Button):
    """Special refresh button class.
    """
    refresh_type = StringProperty("manual")


class AbisuiteScreenButtons(GridLayout):
    """The buttons for the abisuite screen.
    """
    abisuite_monitored_calculations = ObjectProperty(None)
    refresh_button = ObjectProperty(None)


class AbisuiteScreenColumnsTitles(RelativeLayout):
    """The column titles for the monitored calculations.
    """
    calcs_label = ObjectProperty(None)
    project_label = ObjectProperty(None)
