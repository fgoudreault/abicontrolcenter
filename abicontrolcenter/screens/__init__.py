from collections import OrderedDict
import os

from .abisuite import __ALL_ABISUITE_DATA__
from .friends import __ALL_FRIENDS_DATA__
from .pomodoro import PomodoroScreen, __ALL_POMODORO_DATA__
from .settings import AbiControlCenterSettingsScreen, __ALL_SETTINGS_DATA__
from .spotify import __ALL_SPOTIFY_DATA__
from .statistics import __ALL_STATISTICS_DATA__
from ..exceptions import DevError

__ALL_SCREENS_DATA__ = {
        "abisuite": __ALL_ABISUITE_DATA__,
        "friends": __ALL_FRIENDS_DATA__,
        "pomodoro": __ALL_POMODORO_DATA__,
        "spotify": __ALL_SPOTIFY_DATA__,
        "statistics": __ALL_STATISTICS_DATA__,
        "settings": __ALL_SETTINGS_DATA__,
        }

# the key here should match the 'name' in the screen.kv file.
__ALL_SCREENS__ = OrderedDict()

__ALL_SCREENS_KV_FILES__ = []
__ALL_SCREENS_SETTINGS__ = OrderedDict()
__ALL_SCREENS_DEFAULT_SETTINGS__ = OrderedDict()
for dirname, data in __ALL_SCREENS_DATA__.items():
    for dtype in ("screen_cls", "kv_files", "settings", "default_settings"):
        if dtype not in data:
            raise DevError(
                    f"Missing dtype '{dtype}' in screen data '{dirname}'.")
    __ALL_SCREENS__[dirname] = data["screen_cls"]
    __ALL_SCREENS_KV_FILES__ += [os.path.join(dirname, kvfile)
                                 for kvfile in data["kv_files"]]
    __ALL_SCREENS_SETTINGS__[dirname] = data["settings"]
    __ALL_SCREENS_DEFAULT_SETTINGS__.update(data["default_settings"])
