import json

from .spotify_screen import SpotifyScreen


__ALL_SPOTIFY_DATA__ = {
        "kv_files": [
            "spotify_screen.kv",
            ],
        "screen_cls": SpotifyScreen,
        "settings": json.dumps([]),
        "default_settings": {},
        }
