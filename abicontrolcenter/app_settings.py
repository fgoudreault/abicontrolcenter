from collections import OrderedDict
import json
import math

from screeninfo import get_monitors

from .menu_bar import (
        __ALL_MENU_BAR_SETTINGS__, __ALL_MENU_BAR_DEFAULT_SETTINGS__,
        )
from .screens import __ALL_SCREENS_SETTINGS__, __ALL_SCREENS_DEFAULT_SETTINGS__
from .updates import __ALL_UPDATES_SETTINGS__, __ALL_UPDATES_DEFAULT_SETTINGS__


__MENU_BAR_WIDTH__ = 150
__MENU_LABEL_BACKGROUND_COLOR__ = (0.0, 0.0, 1.0, 0.4)
__MENU_BAR_BUTTON_HEIGHT__ = 40
__POPUP_SIZE__ = (300, 300)

__MAX_WINDOW_WIDTH__ = int(
        math.floor(0.9 * min([m.width for m in get_monitors()])))
__MAX_WINDOW_HEIGHT__ = int(
        math.floor(0.9 * min([m.height for m in get_monitors()])))


# APP SETTINGS
# general settings
__ALL_GENERAL_SETTINGS__ = json.dumps([
        {"type": "title",
         "title": "Window"},
        {"type": "numeric",
         "title": "Window width",
         "desc": "Sets the default window width on startup.",
         "key": "window_width",
         "section": "general.window",
         },
        {"type": "numeric",
         "title": "Window height",
         "desc": "Sets the default window height on startup.",
         "key": "window_height",
         "section": "general.window",
         },
        ])


# this dictionary contains the settings information for each panels
# the order of this dictionary dictates the order of the settings panels
__ALL_SETTINGS__ = OrderedDict()
__ALL_SETTINGS__["General"] = __ALL_GENERAL_SETTINGS__
__ALL_ABICC_DEFAULT_SETTINGS__ = {
        # General
        "general.window": {
            "window_width": 800,
            "window_height": 600,
            "window_left": 300,  # invisible to user
            "window_top": 300,   # invisible to user
            }
        }
# Add Menu bar settings
__ALL_SETTINGS__["Menu bar"] = __ALL_MENU_BAR_SETTINGS__
__ALL_ABICC_DEFAULT_SETTINGS__.update(__ALL_MENU_BAR_DEFAULT_SETTINGS__)
# add updates settings
__ALL_SETTINGS__["Updates"] = __ALL_UPDATES_SETTINGS__
__ALL_ABICC_DEFAULT_SETTINGS__.update(__ALL_UPDATES_DEFAULT_SETTINGS__)
# add screens settings and default settings
for screen_name, screen_settings in __ALL_SCREENS_SETTINGS__.items():
    __ALL_SETTINGS__[screen_name.capitalize()] = screen_settings
__ALL_ABICC_DEFAULT_SETTINGS__.update(__ALL_SCREENS_DEFAULT_SETTINGS__)
