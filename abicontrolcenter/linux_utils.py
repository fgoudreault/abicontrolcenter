import os


class cd:
    """Context manager for changing the current working directory.
    Taken from https://stackoverflow.com/a/13197763/6362595

    Usage:
        with cd(path):
            do stuff
    """

    def __init__(self, newPath):
        self.newPath = os.path.abspath(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def touch(filepath):
    """Creates an empty file at the given path.

    Parameters
    ----------
    filepath: str
        The path where the file will be created.
    """
    if os.path.isfile(filepath):
        raise FileExistsError(filepath)
    if os.path.isdir(filepath):
        raise IsADirectoryError(filepath)
    with open(filepath, "w"):
        pass
