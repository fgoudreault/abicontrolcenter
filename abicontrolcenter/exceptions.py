class DevError(Exception):
    """Exception class for any development errors."""
    pass
