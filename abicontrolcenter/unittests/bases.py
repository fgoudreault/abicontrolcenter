from kivy.tests.common import GraphicUnitTest

from abicontrolcenter import AbiControlCenterApp


class BaseAbiControlCenterTest(GraphicUnitTest):
    """Base class for abicc unittest.
    """

    def _app_prerun(self):
        """Implement this function to manipulate the app before the
        run method is called.
        """
        pass

    def setUp(self):
        super().setUp()
        # don't allow remotes because if some are defined, unittests might
        # take a long time!
        self.app = AbiControlCenterApp(
                allow_remotes=False,
                abisuite_screen_refresh_on_start=False,
                )
        self._app_prerun()
        self.app.run()

    def tearDown(self):
        super().tearDown()
        self.app.stop()
        del self.app
