from ..bases import BaseAbiControlCenterTest


class TestPomodoroScreen(BaseAbiControlCenterTest):
    """Test case for the pomodoro screen.
    """
    def setUp(self):
        super().setUp()
        self.pomodoro_screen = self.app.screen_manager.get_screen("pomodoro")

    def test_switch_onoff_sound_alarm(self):
        """Test that switching on and off the sound alarm works.
        """
        # switch on
        self.app.config.set("pomodoro.alarm", "pomodoro_alarm", "1")
        self.assertTrue(
                self.pomodoro_screen.pomodoro_timer.alarm.sound_alarm_on)
        # switch off
        self.app.config.set("pomodoro.alarm", "pomodoro_alarm", "0")
        self.assertFalse(
                self.pomodoro_screen.pomodoro_timer.alarm.sound_alarm_on)
