from ..bases import BaseAbiControlCenterTest


class TestSettings(BaseAbiControlCenterTest):
    """TestCase for the settings.
    """

    def test_closing_settings(self):
        """Test that closing settings switches back to the last screen.
        """
        curr = self.app.screen_manager.current
        # check that the current screen is not the settings
        self.assertNotEqual(curr, "settings")
        # switch to settings
        self.app.open_settings()
        # check that settings is correctly open
        self.assertEqual(self.app.screen_manager.current, "settings")
        # close settings and check we swtiched back to the last screen
        self.app.close_settings()
        self.assertEqual(self.app.screen_manager.current, curr)
