import os
import tempfile
import unittest

from kivy.tests.common import GraphicUnitTest
from kivy.app import runTouchApp

from ... import __ABISUITE_AVAILABLE__, _load_custom_behaviors, _load_kv_files
# from ...abicc_app import AbiControlCenterMainWindow
from ...screens import AbisuiteScreen


@unittest.skipIf(
        not __ABISUITE_AVAILABLE__, "Abisuite not available on system.")
class TestAbisuiteScreen(GraphicUnitTest):
    """Test case for the abisuite screen.
    """

    def setUp(self):
        super().setUp()
        self.tempdir = tempfile.TemporaryDirectory()
        # create a temporary DB
        from abisuite.databases import AbiDB
        self.abidb = AbiDB()
        self.abidb.create_database(os.path.join(self.tempdir.name, "abidb.db"))
        _load_kv_files()
        _load_custom_behaviors()
        self.abisuite_screen = AbisuiteScreen()
        mc = self.abisuite_screen.abisuite_monitored_calculations
        mc.allow_remotes = False
        mc.show_empty_remotes = True
        runTouchApp(self.abisuite_screen)
        from kivy.base import EventLoop
        EventLoop.ensure_window()
        # window = EventLoop.window
        # self.abisuite_screen = self.abicc.screen_manager.abisuite_screen
        # set local db to this db
        self.monitored_calculations = mc
        self.monitored_calculations.databases["local"]["abidb"] = self.abidb

    def tearDown(self):
        super().tearDown()
        self.tempdir.cleanup()
        del self.tempdir
        del self.abisuite_screen
        del self.abidb

    def test_refresh_works(self):
        """Test that the refresh action works.

       Create a calculation in a temporary DB, then check that it has been
        added to the abisuite screen once we hit the refresh button.
        """
        # call refresh and check that there are only one remote (local)
        # and no calculations in it
        mc = self.abisuite_screen.abisuite_monitored_calculations
        self.abisuite_screen.refresh(background=False)
        # check that local db has no calculations in it
        self.assertEqual(len(mc.databases), 1)
        self.assertIn("local", mc.databases)
        self.assertEqual(len(mc.calculation_lines), 1)
        self.assertEqual(len(mc.calculation_lines["local"]), 0)
        self.assertEqual(len(mc.remote_lines), 1)
        self.assertIn("local", mc.remote_lines)
        # artifically add a calculation to the db and refresh screen.
        # inspired from the abisuite unittests
        from abisuite.unittests.routines_for_tests import copy_calculation
        from abisuite.unittests.variables_for_tests import QEPWCalc
        calcpath = os.path.join(self.tempdir.name, "test")
        copy_calculation(QEPWCalc, calcpath)
        self.abidb.add_calculation(calcpath)
        mc.refresh(background=False)
        # check that the calculation has been added
        self.assertEqual(len(mc.calculation_lines["local"]), 1)

    def test_hiding_empty_remotes(self):
        # test the option to hide the empty remotes
        # set to on and check the remote line exists even though there are
        # no calculations
        self.monitored_calculations.show_empty_remotes = True
        self.monitored_calculations.add_locally_available_remotes()
        self.assertTrue(
                self.monitored_calculations.remote_lines["local"] is not None)
        # set to false and make sure the remote line is None
        self.monitored_calculations._show_empty_remotes = False
        self.monitored_calculations.add_locally_available_remotes()
        self.assertTrue(
                self.monitored_calculations.remote_lines["local"] is None)

    def test_hiding_unconnected_remotes(self):
        # artificially change the online status of the local db to false
        self.monitored_calculations.databases["local"]["online"] = False
        # and check that no remotes are drawn
        self.monitored_calculations.show_unconnected_remotes = True
        self.monitored_calculations.add_locally_available_remotes()
        self.assertTrue(
                self.monitored_calculations.remote_lines["local"] is not None)
        # check when hiding them
        self.monitored_calculations.show_unconnected_remotes = False
        self.monitored_calculations.add_locally_available_remotes()
        self.assertTrue(
                self.monitored_calculations.remote_lines["local"] is None)
