from kivy.properties import ObjectProperty

from .update_utils import update_available
from ..bases import BasePopup


class UpdateCheckingPopup(BasePopup):
    """Checking for update popup. Just warns user that we are checking
    for updates.
    """
    text_label = ObjectProperty(None)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._update_available = None

    @property
    def update_available(self):
        """Is True if some updates are available.
        """
        if self._update_available is not None:
            return self._update_available
        self._update_available = update_available()
        return self.update_available

    def open(self):
        super().open("Update Checkup")
