import os

from kivy.factory import Factory
from kivy.lang import Builder
# try importing abisuite. if it works it's available!
try:
    # import abisuite
    # __ABISUITE_AVAILABLE__ = True
    __ABISUITE_AVAILABLE__ = False  # broken for now
except (ImportError, ModuleNotFoundError):
    __ABISUITE_AVAILABLE__ = False


from .abicc_app import AbiControlCenterApp


def _load_kv_files():
    """This function loads all the kv files needed for the
    AbiControlCenterApp.
    """
    
    from ._paths import (
            __SCREENS_PACKAGE_ROOT__, __MENU_BAR_PACKAGE_ROOT__,
            __UPDATES_PACKAGE_ROOT__,  __ABICONTROLCENTER_PACKAGE_ROOT__,
            )
    from .screens import __ALL_SCREENS_KV_FILES__
    from .updates import __ALL_UPDATES_KV_FILES__
    from .menu_bar import __ALL_MENU_BAR_KV_FILES__
    
    __ALL_ABICC_KV_FILES__ = [
            "custom_widgets.kv",
            ]

    roots = [__ABICONTROLCENTER_PACKAGE_ROOT__, __MENU_BAR_PACKAGE_ROOT__,
             __SCREENS_PACKAGE_ROOT__, __UPDATES_PACKAGE_ROOT__,
             ]
    files = [__ALL_ABICC_KV_FILES__, __ALL_MENU_BAR_KV_FILES__,
             __ALL_SCREENS_KV_FILES__, __ALL_UPDATES_KV_FILES__
             ]
    for root, file_list in zip(roots, files):
        for file_path in file_list:
            Builder.load_file(os.path.join(root, file_path))
    # Builder.load_file(os.path.join(__ABICONTROLCENTER_PACKAGE_ROOT__,
    #                                "abicontrolcenter.kv"))


def _load_custom_behaviors():
    """This function loads custom behaviors into the factory.
    """
    from .custom_widgets import HoverBehavior
    Factory.register("HoverBhavior", HoverBehavior)
