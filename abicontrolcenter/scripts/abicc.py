import argparse
import logging
import os
import pathlib
import sys
import time
import traceback


class AbiccArgParser:
    def __init__(self):
        self.parser = self.create_parser()

    def parse_args(self):
        return self.parser.parse_args()

    def parse_known_args(self):
        return self.parser.parse_known_args()

    def create_parser(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
                "--no-allow-remotes", action="store_false",
                help="Do not allow remotes.")
        parser.add_argument(
                "--no-abisuite-screen-refresh-on-start",
                action="store_false",
                help="Do not start app with an automatic abisuite refresh.")
        parser.add_argument(
                "--no-redirect",
                action="store_false",
                help=(
                    "If not specified, the stderr will be redirected to a log "
                    "file. Use --redirect-file option to specify it.")
                )
        parser.add_argument(
                "--redirect-file",
                action="store", default="~/.abicontrolcenter/abicc",
                help=(
                    "Where to store the redirected output if --no-redirect is "
                    "not specified. The time is appended "
                    "to the filename."))
        return parser


if __name__ == "__main__":
    parser = AbiccArgParser()
    args, rest = parser.parse_known_args()
    sys.argv[1:] = rest  # hack because kivy parses args directly from sys.argv
    allow_remotes = args.no_allow_remotes
    automatic_refresh = args.no_abisuite_screen_refresh_on_start
    redirect = args.no_redirect
    redirect_file = os.path.expanduser(args.redirect_file)
    time = time.strftime(
            "%Y.%m.%d-%H.%M.%S",
            time.localtime()).replace(" ", "_").replace(":", "-")
    redirect_file += ".stderr." + time

    def init(**kwargs):
        from abicontrolcenter import AbiControlCenterApp
        AbiControlCenterApp(**kwargs).run()

    kwargs = dict(allow_remotes=allow_remotes,
                  abisuite_screen_refresh_on_start=automatic_refresh)
    try:
        if redirect:
            # add a handler to the kivy logger to log to another file
            # kivy logging must be enabled in settings
            handler = logging.FileHandler(
                    redirect_file.replace("stderr", "stdout"))
            handler.setFormatter(
                    logging.Formatter("%(levelname)s %(message)s"))
            from kivy import Logger as logger
            logger.addHandler(handler)
        init(**kwargs)
    except Exception as e:
        if redirect:
            # write crash data to logging file
            dir_ = os.path.dirname(redirect_file)
            if not os.path.exists(dir_):
                pathlib.Path(dir_).mkdir(parents=True)
            with open(redirect_file, "a") as f:
                traceback.print_exc(file=f)
            print(f"Error happened: traceback written to '{redirect_file}'.")
