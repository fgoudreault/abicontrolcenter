from kivy.uix.popup import Popup
from kivy.uix.relativelayout import RelativeLayout


class BasePopup(RelativeLayout):
    """Base class for a popup window.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.popupWindow = None

    @property
    def is_popup_open(self):
        return self.popupWindow is not None

    def dismiss(self):
        """Dismisses the popup if it's open.
        """
        if not self.is_popup_open:
            # nothing to do
            return
        self.popupWindow.dismiss()
        del self.popupWindow
        self.popupWindow = None

    def open(self, title, size=None, auto_dismiss=False):
        """Opens the popup window.

        Parameters
        ----------
        title: str
            The popup title.
        size: list-like, optional
            The size of the popup. If None, the default is defined in the
            global app_settings.py file.
        auto_dismiss: bool, optional
            If True, you can click outside the popup to dismiss it.
        """
        if size is None:
            from .app_settings import __POPUP_SIZE__
            size = __POPUP_SIZE__
        self.popupWindow = Popup(
                title=title, content=self, auto_dismiss=auto_dismiss,
                size_hint=(None, None), size=size)
        self.popupWindow.open()
