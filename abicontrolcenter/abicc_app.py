import datetime
import json
import os
import pathlib

from kivy import Logger
from kivy.app import App
from kivy.clock import Clock
from kivy.uix.widget import Widget
from kivy.uix.screenmanager import ScreenManager
from kivy.properties import ObjectProperty
from kivy.core.window import Window
from kivy.utils import platform as __PLATFORM__

if __PLATFORM__ == 'linux':
    from Xlib.display import Display
    from Xlib import X

from . import __ABISUITE_AVAILABLE__
from .app_settings import (
        __ALL_SETTINGS__, __ALL_ABICC_DEFAULT_SETTINGS__,
        __MAX_WINDOW_WIDTH__, __MAX_WINDOW_HEIGHT__,
        )
from ._paths import __ABICC_LOGO_DIR__
from .routines import get_user_config, set_user_config
from .screens import __ALL_SCREENS__
from .screens.settings import (
        AbiControlCenterSettingsScreen,  # , SettingsButton
        MaxWindowSizeExceededPopup,
        __CUSTOM_SETTINGS_TYPES__,
       )
from .updates import check_updates, UpdateCheckingPopup


class AbiControlCenterScreenWindow(ScreenManager):
    """Screen manager window for the app.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # if abisuite is available, check it is in the screen order config
        screen_order = get_user_config("menu-bar.buttons", "order", dtype=list)
        if __ABISUITE_AVAILABLE__ and "abisuite" not in screen_order:
            # add abisuite as the first
            screen_order = ["abisuite"] + screen_order
            set_user_config("menu-bar.buttons", "order",
                            screen_order, dtype=list)
        # check if a new screen was added since last time app was updated
        new_screen_order = screen_order.copy()
        for screen_name in __ALL_SCREENS__:
            if screen_name in screen_order:
                # this one is good
                continue
            # this screen was not there previously. add it to the end
            new_screen_order.append(screen_name)
        # reset user config if necessary
        if len(new_screen_order) != len(screen_order):
            set_user_config(
                    "menu-bar.buttons", "order", new_screen_order, dtype=list)
        for screen_name in new_screen_order:
            if screen_name == "abisuite" and not __ABISUITE_AVAILABLE__:
                screen = None
            else:
                screen = __ALL_SCREENS__[screen_name]()
            setattr(self, screen_name + "_screen", screen)
            if screen is not None:
                self.add_widget(screen)


class AbiControlCenterMainWindow(Widget):
    """AbiControlCenter main window. Contains the menu bar and the screen
    manager.
    """
    screen_manager = ObjectProperty(None)
    menu_bar = ObjectProperty(None)


class AbiControlCenterApp(App):
    """AbiControlCenterApp object.

    Parameters
    ----------
    allow_remotes: bool, optional
        If True, allow remote databases to be downloaded.
    abisuite_screen_refresh_on_start: bool, optional
        If True, the abisuite screen is refreshed on startup.
    """

    def __init__(
            self,
            *args,
            allow_remotes=True,
            abisuite_screen_refresh_on_start=True,
            **kwargs):
        self.allow_remotes = allow_remotes
        self.abisuite_screen_refresh_on_start = True
        super().__init__(*args, **kwargs)
        self._window_data_saved_on_stop = False
        # last scr visited before switching to settings
        self._scr_before_settings = None
        # load all kv files of the project
        self._load_kv_files()
        # load behaviors
        self._load_custom_behaviors()

    @property
    def menu_bar(self):
        return self._abiccMainWindow.menu_bar

    @property
    def screen_manager(self):
        return self._abiccMainWindow.screen_manager

    @property
    def screen_order(self):
        return get_user_config("menu-bar.buttons", "order", dtype=list)

    @property
    def abisuite_screen(self):
        return self.screen_manager.abisuite_screen

    @property
    def pomodoro_screen(self):
        return self.screen_manager.pomodoro_screen

    def build(self):
        # set the application icon
        self.icon = os.path.join(__ABICC_LOGO_DIR__, "app_icon.png")
        self.title = "AbiControlCenter"
        # set the settings template
        # self.settings_cls = SettingsWithTabbedPanel
        self.settings_cls = AbiControlCenterSettingsScreen
        # set the main application window
        self._abiccMainWindow = AbiControlCenterMainWindow()
        # adjust window if necessary
        self._adjust_window()
        return self._abiccMainWindow

    def build_config(self, config):
        for section, keyvalues in __ALL_ABICC_DEFAULT_SETTINGS__.items():
            config.setdefaults(section, keyvalues)

    def build_settings(self, settings):
        # first add the custom settings types
        for name, typ in __CUSTOM_SETTINGS_TYPES__.items():
            settings.register_type(name, typ)
        # add the settings panels data
        for panel_title, panel_data in __ALL_SETTINGS__.items():
            if panel_data == "[]":
                # if no settings to show, skip it
                continue
            settings.add_json_panel(
                    panel_title, self.config, data=panel_data)
        # add settings to screen
        set_scr = self.screen_manager.get_screen("settings")
        set_scr.add_widget(settings)

    def get_application_config(self):
        return super().get_application_config(
                os.path.expanduser(
                    os.path.join("~", ".config", "abicontrolcenter.ini")))

    def close_settings(self, *args):
        # compute direction of transition
        mb = self.menu_bar
        self.screen_manager.transition.direction = mb.get_transition_direction(
                "settings", self._scr_before_settings)
        # change from menu bar
        self.menu_bar.current_screen = self._scr_before_settings

    def display_settings(self, settings):
        self._scr_before_settings = self.screen_manager.current
        mb = self.menu_bar
        self.screen_manager.transition.direction = mb.get_transition_direction(
                self._scr_before_settings, "settings")
        self.screen_manager.current = "settings"

    def on_config_change(self, config, section, key, value):
        # when we change the settings, some of them can be applied immediately
        # but they require to trigger some functions somewhere in the code
        # thie method triggers them
        super().on_config_change(config, section, key, value)
        pomodoro_scr = self.pomodoro_screen
        if section == "pomodoro.work-and-break-times":
            # reset timers
            pomodoro_scr.pomodoro_control_bar.reset_button.trigger_action()
            pomodoro_scr.pomodoro_timer.reset()
        elif section == "pomodoro.appearance":
            pomodoro_scr.pomodoro_timer.circle_color = list(
                    json.loads(value))
        elif section == "general.window":
            # window size have changed. check we did not overpassed the maximum
            if key == "window_width" and int(value) > __MAX_WINDOW_WIDTH__:
                # issue warning and set max value
                popup = MaxWindowSizeExceededPopup("width")
                popup.open()
                # set to maximum value
                set_user_config(section, key, __MAX_WINDOW_WIDTH__, dtype=int)
                super().on_config_change(
                        config, section, key, __MAX_WINDOW_WIDTH__)
            elif key == "window_height" and int(value) > __MAX_WINDOW_HEIGHT__:
                popup = MaxWindowSizeExceededPopup("height")
                popup.open()
                # set to maximum value
                set_user_config(section, key, __MAX_WINDOW_HEIGHT__, dtype=int)
                super().on_config_change(
                        config, section, key, __MAX_WINDOW_HEIGHT__)
            # window config was changed. apply changes
            self._adjust_window()
        elif section == "abisuite.appearance":
            # the show_empty_remote have been modified
            # reset the corresponding attribute from the widget
            # and reset the screen
            mc = self.abisuite_screen.abisuite_monitored_calculations
            mc.show_empty_remotes = None  # reset
            mc.show_unconnected_remotes = None  # reset
            mc.add_locally_available_remotes()
        elif section == "abisuite.refresh":
            if key == "automatic_refresh_frequency":
                # perform an automatic_refresh to see if we need to refresh
                # for instance if user decreases the refresh rate
                self.abisuite_screen.automatic_refresh()
        elif section == "menu-bar.buttons" and key == "order":
            # the menu bar buttons order has changed, recreate them
            self.menu_bar.screen_buttons.create_menu_bar_buttons()

    def on_start(self):
        if __PLATFORM__ == 'linux':
             self.set_wm_class()
        # check if we need to check for updates
        self._check_for_updates()
        # disable abisuite stuff if needed
        from . import __ABISUITE_AVAILABLE__
        if not __ABISUITE_AVAILABLE__:
            self._disable_abisuite()
        else:
            try:
                # refresh abisuite screen
                # show locally available data
                mc = self.abisuite_screen.abisuite_monitored_calculations
                mc.add_locally_available_remotes()
                if self.abisuite_screen_refresh_on_start:
                    # refresh screen
                    self.abisuite_screen.refresh(_automatic=True)
            except Exception:
                import traceback
                __ABISUITE_AVAILABLE__ = False
                self._disable_abisuite()
                traceback.print_exc()
        # if first screen is settings, open them
        screen_order = get_user_config("menu-bar.buttons", "order", dtype=list)
        if screen_order[0] == "settings" or (not __ABISUITE_AVAILABLE__ and
                                             screen_order[1] == "settings" and
                                             screen_order[0] == "abisuite"):
            # open settings
            self.open_settings()

    def on_stop(self):
        if __ABISUITE_AVAILABLE__:
            # kill refresh processes if any
            monitored = self.abisuite_screen.abisuite_monitored_calculations
            monitored.kill_processes()
        if not self._window_data_saved_on_stop:
            # added this if statement cause it seems the stop() method is
            # always called twice for some reason.
            # save window data to keep the same window size/pos next
            # time we open the app
            width, height = Window.size
            set_user_config("general.window", "window_width", width, dtype=int)
            set_user_config(
                    "general.window", "window_height", height, dtype=int)
            try:
                left = Window.left
                top = Window.top
                set_user_config(
                        "general.window", "window_left", left, dtype=int)
                set_user_config("general.window", "window_top", top, dtype=int)
            except Exception as e:
                Logger.error("Couldn't save window pos for some reason...")
                Logger.exception(e)
            self._window_data_saved_on_stop = True

    def set_wm_class(self):
        """
        Set the X11 WM_CLASS. This is used to link the window to the X11
        application (menu entry from the .desktop file). Gnome-shell won't
        display the application icon correctly in the dash with the default
        value of `python3, python3`.
        """
        display = Display()
        root = display.screen().root
        windowIDs = root.get_full_property(
                display.intern_atom('_NET_CLIENT_LIST'), X.AnyPropertyType).value
        for windowID in windowIDs:
            window = display.create_resource_object('window', windowID)
            title = window.get_wm_name()
            if title == self.title:
                window.set_wm_class(self.title, "python3")
                display.sync()

    def _adjust_window(self):
        """Adjust the window size according to the user settings.
        """
        height = get_user_config("general.window", "window_height", dtype=int)
        width = get_user_config("general.window", "window_width", dtype=int)
        Window.size = (width, height)
        left = get_user_config("general.window", "window_left", dtype=int)
        top = get_user_config("general.window", "window_top", dtype=int)
        try:
            Window.left = left
            Window.top = top
        except Exception as e:
            # an error occured, continue as this is not critical
            # but print the error
            Logger.error("Couldn't adjust window pos for some reason...")
            Logger.exception(e)

    def _check_for_updates(self):
        freq = get_user_config("updates", "updates_frequency")
        if freq == "never":
            return
        if get_user_config("updates", "updates_opening_checkup") != "1":
            return
        last_update_checking = datetime.datetime.fromtimestamp(
                float(get_user_config("updates", "last_update_checking")))
        delta = datetime.datetime.now() - last_update_checking
        delta = delta.total_seconds()
        if freq == "every hour" and delta < 3600:
            return
        elif freq == "every day" and delta < 86400:
            return
        elif freq == "every week" and delta < 604800:
            return
        elif freq == "every month" and delta < 2592000:  # 30 days
            return
        # we need to check for an update!
        # first open the checking update popup
        update_checkup_popup = UpdateCheckingPopup()
        update_checkup_popup.open()
        # then check updates 2 sec
        # after 2sec, app should be running
        Clock.schedule_once(
                lambda dt: check_updates(update_checkup_popup), 2)
        # set config
        set_user_config(
                "updates", "last_update_checking",
                json.dumps(datetime.datetime.now().timestamp()))

    def _disable_abisuite(self):
        # remove the abisuite button in the menu bar
        if hasattr(self.menu_bar.screen_buttons,
                   "button_for_abisuite_screen"):
            self.menu_bar.screen_buttons.remove_widget(
                    self.menu_bar.screen_buttons.button_for_abisuite_screen)
        # if abisuite is first in screen order, set the second screen button
        screen_order = get_user_config("menu-bar.buttons", "order", dtype=list)
        if "abisuite" == screen_order[0]:
            btn = getattr(self.menu_bar.screen_buttons,
                          f"button_for_{screen_order[1]}_screen")
            btn.state = "down"
        # self.menu_bar.screen_buttons.button_for_pomodoro_screen.state
        # = "down"
        __ALL_SETTINGS__.pop("Abisuite")

    def _load_custom_behaviors(self):
        """Load all the custom behaviors of the project.
        """
        from . import _load_custom_behaviors
        _load_custom_behaviors()

    def _load_kv_files(self):
        """Load all the kv files of the project.
        """
        from . import _load_kv_files
        _load_kv_files()
