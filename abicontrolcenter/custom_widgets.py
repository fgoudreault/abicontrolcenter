from kivy.graphics import Line, Color
from kivy.properties import (
        BooleanProperty, ListProperty, NumericProperty,
        )
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.core.window import Window


class BackgroundColor(Widget):
    """Widget with a background color.
    """
    background_color = ListProperty([1, 1, 1, 1])


class Contour(Widget):
    """A widget which has a contour.
    """
    contour_color = ListProperty([1, 1, 1, 1])
    contour_width = NumericProperty(1.0)
    contour_bottom = BooleanProperty(False)
    contour_top = BooleanProperty(False)
    contour_left = BooleanProperty(False)
    contour_right = BooleanProperty(False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with self.canvas:
            Color(self.contour_color)
            if self.contour_top:
                # draw top contour
                self.top_line = Line(
                        points=self.top_left_corner + self.top_right_corner,
                        width=self.contour_width,
                        cap="round",
                        joint="round",
                        close=False)
                self.bind(pos=self.update_top_line, size=self.update_top_line)
            if self.contour_bottom:
                # draw bottom contour
                self.bottom_line = Line(
                     points=self.bottom_left_corner + self.bottom_right_corner,
                     width=self.contour_width,
                     cap="round",
                     joint="round",
                     close=False)
                self.bind(pos=self.update_bottom_line,
                          size=self.update_bottom_line)
            if self.contour_left:
                # draw left contour
                self.left_line = Line(
                     points=self.bottom_left_corner + self.top_left_corner,
                     width=self.contour_width,
                     cap="round",
                     joint="round",
                     close=False)
                self.bind(pos=self.update_left_line,
                          size=self.update_left_line)
            if self.contour_right:
                # draw right contour
                self.right_line = Line(
                     points=self.bottom_right_corner + self.top_right_corner,
                     width=self.contour_width,
                     cap="round",
                     joint="round",
                     close=False)

    @property
    def bottom_left_corner(self):
        return [self.x, self.y]

    @property
    def bottom_right_corner(self):
        return [self.right, self.y]

    @property
    def top_left_corner(self):
        return [self.x, self.top]

    @property
    def top_right_corner(self):
        return [self.right, self.top]

    def update_bottom_line(self, *args):
        self.bottom_line.points = (
                self.bottom_left_corner + self.bottom_right_corner
                )

    def update_top_line(self, *args):
        self.top_line.points = self.top_left_corner + self.top_right_corner

    def update_left_line(self, *args):
        self.left_line.points = self.bottom_left_corner + self.top_left_corner

    def update_right_line(self, *args):
        self.right_line.points = (
                self.bottom_right_corner + self.top_right_corner
                )

    @staticmethod
    def extract_kwargs(**kwargs):
        """Splits the kwargs into 2 dicts. First one is the Contour kwargs
        and the second is the rest.
        """
        contour_kw = ["contour_" + x for x in (
            "top", "bottom", "left", "right", "width", "color")]
        newkw = {}
        oldkw = kwargs.copy()
        for kw in contour_kw:
            if kw in oldkw:
                newkw[kw] = kwargs.pop(kw)
        return newkw, kwargs


class ContouredLabel(Label, Contour):
    """A label on which you can set a contour.
    """
    def __init__(self, *args, **kwargs):
        new_kw, other_kw = Contour.extract_kwargs(**kwargs)
        Label.__init__(self, *args, **other_kw)
        Contour.__init__(self, *args, **new_kw)


# FIXME: This does not work for some reason...
class ContouredCheckBox(CheckBox, Contour):
    """A checkbox on which you can set a contour.
    """
    def __init__(self, *args, **kwargs):
        new_kw, other_kw = Contour.extract_kwargs(**kwargs)
        CheckBox.__init__(self, *args, **other_kw)
        Contour.__init__(self, *args, **new_kw)


# taken from:
# https://gist.github.com/opqopq/15c707dc4cffc2b6455f
class HoverBehavior:
    """Behavior class for when hovering a widget with the mouse.
    """
    hovered = BooleanProperty(False)

    def __init__(self, **kwargs):
        Window.bind(mouse_pos=self.on_mouse_pos)
        super(HoverBehavior, self).__init__(**kwargs)

    def on_mouse_pos(self, *args):
        if not self.get_root_window():
            return  # do proceed if I'm not displayed <=> If have no parent
        pos = args[1]
        self.hovered = self.collide_point(*pos)


class HoverableLabel(Label, HoverBehavior):
    """Label that implements the hover behavior.
    """
    def __init__(self, *args, **kwargs):
        Label.__init__(self, *args, **kwargs)
        HoverBehavior.__init__(self)
