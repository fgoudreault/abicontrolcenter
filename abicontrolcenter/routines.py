import json

from kivy.app import App


def get_user_config(section, key, dtype=None, app=None):
    """Gets the current user settings.

    Parameters
    ----------
    section : str
        The settings section.
    key : str
        The settings key in the section.
    dtype : The class to return the data. If None, the raw data is returned.
    app: Application to get the config from. If None, the global one is loaded.
    """
    if app is None:
        app = App.get_running_app()
        if app is None:
            raise RuntimeError("Running app is None...")
    data = app.config.get(section, key)
    if dtype is not None:
        if "'" in data:
            # data contains quotation marks. better just use eval() instead of
            # json.loads()
            return dtype(eval(data))
        if dtype is bool:
            # special case
            if data in (0, "0", False, "False", "false"):
                return False
            elif data in (1, "1", True, "True", "true"):
                return True
            else:
                raise LookupError(data)
        return dtype(json.loads(data))
    return data


def set_user_config(section, key, value, dtype=None):
    """Sets the user settings to a new value.

    Parameters
    ----------
    section : str
        The settings section.
    key : str
        The settings key in the section.
    value : the new value, must be of same type as previous value.
    dtype: optional
        If not None, specifies the data type. Doing so, a check is done
        to make sure the date saved is of the same type as the data stored.
    """
    if dtype is not None:
        # first get previous value
        prev = get_user_config(section, key, dtype=dtype)
        if type(prev) != type(value):
            raise TypeError(
                    f"New setting '{value}' ({type(value)}) "
                    "not the same type as "
                    f"previous one '{prev}' ({type(prev)}).")
    config = App.get_running_app().config
    config.set(section, key, value)
    config.write()


def quit_app(*args):
    """Quit the app.
    """
    App.get_running_app().stop()
