from setuptools import setup
import os


def post_install():
    # prepend PATH variable with scripts directory
    here = os.path.dirname(os.path.abspath(__file__))
    scripts = os.path.join(here, "abicontrolcenter", "scripts")
    PATH = os.environ["PATH"].split(":")
    if scripts not in PATH:
        # append path to scripts in bashrc
        print(  # noqa: T001
          "The $PATH environment variable has been extended to include"
          " 'abicontrolpackage' scripts.")  # noqa: T001
        print("See '~/.bashrc'")  # noqa: T001
        comment_line = ("# The following line has been added by the "
                        "'abicontrolpackage' setup script.")
        bashrc = os.path.expanduser("~/.bashrc")
        with open(bashrc, "r") as f:
            # first read the file and if this line appears, don't redo it!
            all_lines = f.readlines()
            for line in all_lines:
                if comment_line in line:
                    # break and leave
                    append_path = False
                    break
            else:
                append_path = True
        if append_path:
            with open(bashrc, "a") as f:
                # the comment line was not found, set it in the bashrc
                f.write(comment_line + "\n")
                f.write("\n")
                f.write(f'export PATH="{scripts}:$PATH"')

    # also install a file to put the application in the launcher
    # this is for Linux only
    if os.sys.platform == "linux":
        desktop_file = os.path.expanduser(
                os.path.join("~", ".local", "share", "applications",
                             "abicontrolcenter.desktop"))
        if os.path.exists(desktop_file):
            return
        # need to install in launcher
        from abicontrolcenter.app_settings import (
                __ABICC_APP_DIR__, __ABICC_LOGO_DIR__,
                )
        exec_path = os.path.join(__ABICC_APP_DIR__, "scripts", "abicc")
        icon_path = os.path.join(__ABICC_LOGO_DIR__, "app_icon.png")
        with open(desktop_file, "w") as f:
            f.write(
                f"[Desktop Entry]\nExec={exec_path}\nIcon={icon_path}"
                "\nTerminal="
                f"false\nName=AbiControlCenter\nType=Application\n")
        print("A Launcher shortcut has been created at '{desktop_file}'.")


with open("requirements.txt") as f:
    requirements = f.read().splitlines()
    # remove https links from requirements
    requirements = [dep for dep in requirements if not dep.startswith("https")]

setup(name="abicontrolcenter",
      python_requires=">=3.6",
      license="MIT",
      install_requires=requirements,
      )

post_install()
